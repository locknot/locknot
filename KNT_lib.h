/**********************************************************
 * LT 02-03-2011                                          *
 * KNT_lib: prototypes of all functions related to knot   *
 * localization.                                          *
 * 1. Closure schemes;                                    *
 * 2. Knot matrix determination;                          *
 * 3. knot localization schemes;                          *
 * 4. auxiliary functions.                                *
 *                                                        *
 * KNOWN BUGS                                             *
 *                                                        *
 * KNTLqhull_close_subchain fails if start == end !       *
 *                                                        *
 * ********************************************************/

#ifndef HDR_KNT_LIB
#define HDR_KNT_LIB

//#include "KNT_defaults.h"
#include "KNT_arc.h"
//#include "projections.h"
#include "Alexander.h"
#include "my_geom.h"
#include "my_random.h"
#include "my_memory.h"
#include "messages.h"

#include "qhull_a.h"

#include <stdio.h>
#include <math.h>   /* -std=c99 */
#include <stdlib.h>
#include <search.h>  /* -std=c99 */
#include <gsl/gsl_math.h> //define constants
#include <gsl/gsl_blas.h>
//#include <hdf5.h>


#define DEBUG 0

#define N_KNOWN_PRIMES  14
#define KNOWN_PRIMES   { {K_3_1, KADT_K_3_1},{K_4_1, KADT_K_4_1},{K_5_1, KADT_K_5_1},\
  {K_6_1, KADT_K_6_1},{K_6_2, KADT_K_6_2},{K_6_3, KADT_K_6_3}, \
  {K_7_1, KADT_K_7_1},{K_7_2, KADT_K_7_2} ,{K_7_3, KADT_K_7_3} ,{K_7_4, KADT_K_7_4} ,\
  {K_7_5, KADT_K_7_5},{K_7_6, KADT_K_7_6}, {K_7_7, KADT_K_7_7},{K_5_2,KADT_K_5_2}}
//#include <hdf5_hl.h>
/*
 * structs used only in this library
 */

struct knot_adets {
  int k_id;
  int Adets[2];
  //int Adet_1;
  //int Adet_2;
};

typedef struct knot_adets KNTadets;

struct node {
  KNTadets knot;
  struct node * next;
};

typedef struct node node;

KNTadets pop ( node **head );
void push(node **head, KNTadets knot);


/*
 * Knot determination and knot localization schemes
 */
int     KNTLknot_determination ( KNTarc * knt_ptr, ALX_wspace * alx_wspace );
/* knot matrix evaluation    */
void    KNTLkntmatrix
      ( KNTarc * knt_ptr, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace);
/* knot localization schemes */
void    KNTLloc_shrt_knot_unsafe
      ( KNTarc * knt_ptr, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace);
void    KNTLloc_shrt_knot
      ( KNTarc * knt_ptr, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace);
void    KNTLloc_shrtcnt_knot
      ( KNTarc * knt_ptr, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace);
/* knot localization schemes with simplification aliasing */
void    KNTLloc_shrt_knot_rect
      ( KNTarc * knt_ptr, KNTarc * knt_rect, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace);
void    KNTLloc_shrtcnt_knot_rect
      ( KNTarc * knt_ptr, KNTarc * knt_rect, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace);
void 		KNTLloc_shrt_knot_unsafe_rect
			( KNTarc * knt_ptr, KNTarc * knt_rect, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace );

void 		KNTLloc_shrt_knot_rect1
			( KNTarc * knt_ptr, KNTarc * knt_rect, int st_p, int end_p, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace );
void 		KNTLloc_shrtcnt_knot_rect1 
			( KNTarc * knt_ptr, KNTarc * knt_rect, int st_p, int end_p, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace);
void 		KNTLloc_shrt_knot_unsafe_rect1
			( KNTarc * knt_ptr, KNTarc * knt_rect, int st_p, int end_p, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace );

void    KNTLloc_shrt_knot_rect2
      ( KNTarc * knt_ptr, KNTarc * knt_rect, int st_p, int end_p, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace);
void    KNTLloc_shrtcnt_knot_rect2
      ( KNTarc * knt_ptr, KNTarc * knt_rect, int st_p, int end_p, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace);
void    KNTLloc_shrt_knot_unsafe_rect2
      ( KNTarc * knt_ptr, KNTarc * knt_rect, int st_p, int end_p, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace );
/* MEMORY MANAGEMENT */
void buffer_init ( double ***buff, int buffer_dim ) ;
void free_buffer ( double ***buff);

/* return id of knot from alexander determinants. ids are in KNT_defaults.h */
int  KNTLknot_id ( int dets[2] );
/* return Alexander determinants from knot id. ids are in KNT_defaults.h */
void KNTLkid_to_Adet ( int kid, int *Adet_1, int *Adet_2 ) ;

void   copy_chain              ( double ** coord, int len, double ** coordnew );
/* check if the movement of a bead does not affect topology */
int    check_viable_displacement_prd
       ( double  ** coord, int nsteps, int bead_moved, double *moved_bead_coordinate );
int    check_viable_displacement_sample
       ( double  ** coord, int * sample, int n_sample, int idx_bead_moved, double *moved_bead_coordinate);
/* rectifcation procedures on  chain and KNTarc */
void   chain_remove_beads_new
       ( int oldlen, double ** oldcoord, int *newlen,  double ** coord,  int *index  , int max_stride);
//void chain_remove_beads_w_shuffle
 //      ( int oldlen, double ** oldcoord, int *newlen, double ** coord,  int max_stride);
void chain_remove_beads_w_shuffle_nostride
      ( int oldlen, double ** oldcoord, int *newlen, double ** coord);
int chain_remove_beads_local
       ( int oldlen, double ** oldcoord,int start,int end, int *newlen, double ** coord, int * index,int min_stride, int max_stride);
KNTarc * KNTLrectify_coord ( KNTarc * knt_ptr, int max_stride );
KNTarc * KNTLrectify_coord_local ( KNTarc * knt_ptr, int st_p, int end_p, int min_stride, int max_stride );
/* comparison functions to be called by qsort and lfind */
int compare_len   ( const void *a , const void *b );
int compare_adets ( const void *a , const void *b );
int compare_kid   ( const void *a , const void *b );
int check_inclusion (int start, int end, int st_p, int end_p);

void KNTLreset_random_closure( int N_cls, double R_fact, double tresh );
/* rotate knot to change projection*/
void KNTLrotate ( KNTarc * knt_ptr, double axis[3], double theta);
void KNTLrotate_random ( KNTarc * knt_ptr );
#endif /*HDR_KNT_LIB */
