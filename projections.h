/******************************************************************************
 * 27-12-2010  LT   
 * This header implement frame rotation through quaternions.            
 * The structure ref_frameT contains both the axis and the three angles used 
 * to obtain the actual axis from the canonical base. The pointer next allows
 * the construction of a linked list. 
 * 
 * In situations when a series of random bases is needed, like for example a 
 * series of random projections for the computation of the writhe of a ring,
 * using a linked list has the advantage of allowing a fast and easy check 
 * of every frame used. It can be useful for debugging purposes.
 *
 * The function q_init_frame() create a new frame with a randomly rotate base.
 * add_frame add a random frame to the list passed as his argument.
 * get_frame(ref_frameT *, int n ) extract the n-th frame from the list, if it 
 * exists.
 * free_frame(ref_frameT * rf) free all the memory allocated for the list rf.
 * project_chain(ref_frameT *rf, int N, double **) change the reference frame
 * chain to rf; 
 * q_frame_rotate perform a random rotation and is to be used only in 
 * q_init_frame.
 *****************************************************************************/
#ifndef  HDR_PRJ
#define  HDR_PRJ

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "my_memory.h"
#include "my_random.h"
#include "messages.h"


typedef struct ref_frameT ref_frameT;

struct ref_frameT { 

  double    q_phi,
            q_theta,
            q_psi;

  double    ax[3][3];

  ref_frameT * next;

};


ref_frameT *  q_init_frame      ( );
ref_frameT *  get_frame         ( ref_frameT * rf, int n );
void          add_frame         ( ref_frameT * rf );
void          free_ref_frameT   ( ref_frameT * rf );
void          project_chain     ( ref_frameT * rf, int N,  double ** chain, double ** prj_chain  );
void          q_frame_rotate    ( ref_frameT * rf );

#endif /* HDR_PRj */
