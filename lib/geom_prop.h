#ifndef _GEOM_PROPS_
#define _GEOM_PROPS_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_sort_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_math.h>

#include "nrutil.h"
#include "geom.h"
#include "ran2.h"
#include "melt_utils.h"


void center_of_mass(int N,double coord[N][3], double *cm);
double gyration_radius(int N, double coord[N][3]);
double find_radius(int N,double coord[N][3]); // Origin = center of mass
double find_radius_fixed_origin(int N, double coord[N][3]); //origin = (0,0,0)
void inertia_tensor(int N, double coord[N][3], double mass[N], gsl_matrix* inrt);
double asphericity(gsl_vector *semiaxis);
double prolateness(gsl_vector *semiaxis);

void center_of_mass_nr(int N,double **coord, double *cm);
double gyration_radius_nr(int N, double **coord);
double find_radius_nr(int N,double **coord); // Origin = center of mass
double find_radius_fixed_origin_nr(int N, double **coord); //origin = (0,0,0)
void inertia_tensor_nr(int N, double **coord, double mass[N], gsl_matrix* inrt);

#endif
