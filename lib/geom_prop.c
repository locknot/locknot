#include "geom_prop.h"

void inertia_tensor(int N, double coord[N][3], double mass[N], gsl_matrix* inrt){
  int i;
  double cm[3];
  double cm_coord[MaxLength][3];
  double xx, yy, zz;
  double xy, xz, yz;
  xx=yy=zz=0.0;
  xy=xz=yz=0.0;
  center_of_mass(N,coord,cm);
  for(i=0;i<N;i++){
    cm_coord[i][0]=coord[i][0]-cm[0];
    cm_coord[i][1]=coord[i][1]-cm[1];
    cm_coord[i][2]=coord[i][2]-cm[2];
  }

  for(i=0;i<N;i++){
    xx+=mass[i]*(cm_coord[i][1]*cm_coord[i][1]+cm_coord[i][2]*cm_coord[i][2]);
    yy+=mass[i]*(cm_coord[i][0]*cm_coord[i][0]+cm_coord[i][2]*cm_coord[i][2]);
    zz+=mass[i]*(cm_coord[i][1]*cm_coord[i][1]+cm_coord[i][0]*cm_coord[i][0]);
    xy-=mass[i]*cm_coord[i][0]*cm_coord[i][1];
    xz-=mass[i]*cm_coord[i][0]*cm_coord[i][2];
    yz-=mass[i]*cm_coord[i][1]*cm_coord[i][2];
  }
  gsl_matrix_set(inrt,0,0,xx);
  gsl_matrix_set(inrt,1,1,yy);
  gsl_matrix_set(inrt,2,2,zz);
  gsl_matrix_set(inrt,0,1,xy);
  gsl_matrix_set(inrt,1,0,xy);
  gsl_matrix_set(inrt,0,2,xz);
  gsl_matrix_set(inrt,2,0,xz);
  gsl_matrix_set(inrt,1,2,yz);
  gsl_matrix_set(inrt,2,1,yz);
}

double asphericity(gsl_vector *semiaxis){
  double asph;
  double a,b,c;
  gsl_sort_vector(semiaxis);
  a=gsl_vector_get(semiaxis,2);
  b=gsl_vector_get(semiaxis,1);
  c=gsl_vector_get(semiaxis,0);

  asph=0.5*((a-b)*(a-b)+(a-c)*(a-c)+(b-c)*(b-c))/pow((a+b+c),2);
  return asph;
}

double prolateness(gsl_vector *semiaxis){
  double prol;
  double den;
  double a,b,c;
  gsl_sort_vector(semiaxis);
  a=gsl_vector_get(semiaxis,2);
  b=gsl_vector_get(semiaxis,1);
  c=gsl_vector_get(semiaxis,0);
  den=(a*a+b*b+c*c-a*b-a*c-b*c);
  den=pow(den,3./2.);
  prol=0.5*(2*a-b-c)*(2*b-a-c)*(2*c-a-b)/den;
  return prol;
}


double gyration_radius(int N,double coord[N][3])
{
  int i;
  double cm[3], r2;
  r2=0;
  center_of_mass(N,coord,cm);
  for(i=0;i<N;i++)
    r2+=pow(dist_d(coord[i],cm,3),2);
  r2=r2/N;
  return sqrt(r2);
}
double find_radius(int N,double coord[N][3]) // Origin = center of mass
{
  int i;
  double cm[3], r2, temp_r2;
  center_of_mass(N,coord,cm);
  // The following is to find the farthest vortex from the center of mass (cm),
  // by first computing the cm coordinates, then computing the distances of
  // ALL the vortices to the cm, and finally finding the maximum amongst the
  // distances. To avoid sqrt, which is repeated Len times in each run, it is 
  // better to compute the squared distances then find the maximum amongst them...
  r2 = 0.0;
  for(i=0; i < N; i++)
  {
  	temp_r2= dist_d(coord[i],cm,3);
  	if (temp_r2 > r2) r2 = temp_r2;		
  }
  
  // ... However, remember to return the sqrt of the found squared radius
  return r2;	
}
double find_radius_fixed_origin(int N, double coord[N][3])
{
	int i;
	double r, temp_r;

	r = 0.0;
	for(i=0; i < N; i++)
	{
		temp_r= norm_d(coord[i],3);
		if (temp_r > r) r = temp_r;	
	}
	return (r);

}
void center_of_mass(int N,double coord[N][3], double *cm)
{
	int i;
	cm[0] = cm[1] = cm[2] = 0.0;
	for(i=0; i < N; i++)
	{
		cm[0] += coord[i][0];
		cm[1] += coord[i][1];
		cm[2] += coord[i][2];
	}

	cm[0] /= N;
	cm[1] /= N;
	cm[2] /= N;
}
/**************************************************************************/

void inertia_tensor_nr(int N, double **coord, double mass[N], gsl_matrix* inrt){
  int i;
  double cm[3];
  double **cm_coord;
  double xx, yy, zz;
  double xy, xz, yz;
  xx=yy=zz=0.0;
  xy=xz=yz=0.0;
  cm_coord=(double**)dmatrix(0,N,0,3);
  center_of_mass_nr(N,coord,cm);
  for(i=0;i<N;i++){
    cm_coord[i][0]=coord[i][0]-cm[0];
    cm_coord[i][1]=coord[i][1]-cm[1];
    cm_coord[i][2]=coord[i][2]-cm[2];
  }

  for(i=0;i<N;i++){
    xx+=mass[i]*(cm_coord[i][1]*cm_coord[i][1]+cm_coord[i][2]*cm_coord[i][2]);
    yy+=mass[i]*(cm_coord[i][0]*cm_coord[i][0]+cm_coord[i][2]*cm_coord[i][2]);
    zz+=mass[i]*(cm_coord[i][1]*cm_coord[i][1]+cm_coord[i][0]*cm_coord[i][0]);
    xy-=mass[i]*cm_coord[i][0]*cm_coord[i][1];
    xz-=mass[i]*cm_coord[i][0]*cm_coord[i][2];
    yz-=mass[i]*cm_coord[i][1]*cm_coord[i][2];
  }
  gsl_matrix_set(inrt,0,0,xx);
  gsl_matrix_set(inrt,1,1,yy);
  gsl_matrix_set(inrt,2,2,zz);
  gsl_matrix_set(inrt,0,1,xy);
  gsl_matrix_set(inrt,1,0,xy);
  gsl_matrix_set(inrt,0,2,xz);
  gsl_matrix_set(inrt,2,0,xz);
  gsl_matrix_set(inrt,1,2,yz);
  gsl_matrix_set(inrt,2,1,yz);
  free_dmatrix(cm_coord,0,N,0,3);
}


double gyration_radius_nr(int N,double **coord)
{
  int i;
  double cm[3], r2;
  r2=0;
  center_of_mass_nr(N,coord,cm);
  for(i=0;i<N;i++)
    r2+=pow(dist_d(coord[i],cm,3),2);
  r2=r2/N;
  return sqrt(r2);
}
double find_radius_nr(int N,double **coord) // Origin = center of mass
{
  int i;
  double cm[3], r2, temp_r2;
  center_of_mass_nr(N,coord,cm);
  // The following is to find the farthest vortex from the center of mass (cm),
  // by first computing the cm coordinates, then computing the distances of
  // ALL the vortices to the cm, and finally finding the maximum amongst the
  // distances. To avoid sqrt, which is repeated Len times in each run, it is 
  // better to compute the squared distances then find the maximum amongst them...
  r2 = 0.0;
  for(i=0; i < N; i++)
  {
  	temp_r2= dist_d(coord[i],cm,3);
  	if (temp_r2 > r2) r2 = temp_r2;		
  }
  
  // ... However, remember to return the sqrt of the found squared radius
  return r2;	
}
double find_radius_fixed_origin_nr(int N, double **coord)
{
	int i;
	double r, temp_r;

	r = 0.0;
	for(i=0; i < N; i++)
	{
		temp_r= norm_d(coord[i],3);
		if (temp_r > r) r = temp_r;	
	}
	return (r);

}
void center_of_mass_nr(int N,double **coord, double *cm)
{
	int i;
	cm[0] = cm[1] = cm[2] = 0.0;
	for(i=0; i < N; i++)
	{
		cm[0] += coord[i][0];
		cm[1] += coord[i][1];
		cm[2] += coord[i][2];
	}

	cm[0] /= N;
	cm[1] /= N;
	cm[2] /= N;
}
