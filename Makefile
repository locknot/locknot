#compiler
CC = gcc
#compiler flags
C_FLAGS =  -O3 -g -Wall -std=c99 -fno-omit-frame-pointer
#C_FLAGS =   -ggdb -Wall -std=c99 -fno-omit-frame-pointer
#debug flags
#C_FLAGS = -ggdb -Wall -std=c99
#libraries
C_LIB_LM = -lm
VPATH=./lib
#headers
#export CPATH=/share/apps/include:./lib:./qhull-2012.1/src/libqhull/
#export LIBRARY_PATH=/share/apps/lib:./lib:./qhull-2012.1/lib/
#export CPATH=/opt/local/include:./lib:/share/apps/include:./qhull-2012.1/src/libqhull
#export LIBRARY_PATH=/opt/local/lib:./lib:./qhull-2012.1/lib:/share/apps/lib
DIR_HEADERS=./lib/
QHULL=./qhull-2012.1
QHULLIB=./qhull-2012.1/lib
QHULLHDR=./qhull-2012.1/src/libqhull
#general definitions
OBJ1=my_geom.o my_memory.o messages.o
OBJ2=my_random.o Alexander.o
OBJ3=KNT_arc.o
OBJ4=KNT_closures.o KNT_qhull.o
OBJ5=KNT_lib.o KNT_io.o
OBJS=$(OBJ5) $(OBJ4) $(OBJ3) $(OBJ2) $(OBJ1) $(QHULLIB)/libqhullstatic.a

all:  KNTloc_ring.x KNTloc_linear.x KNTquick_ring.x KNTquick_linear.x KNTclose.x

KNTquick_linear.x: $(OBJS) KNTquick_linear.c
	$(CC) $(C_FLAGS)  -I $(DIR_HEADERS)    -I $(QHULLHDR) $^  -lm  -lgsl -lgslcblas $(QHULLIB)/libqhullstatic.a -o $@
KNTquick_ring.x: $(OBJS) KNTquick_ring.c
	$(CC) $(C_FLAGS)  -I $(DIR_HEADERS)    -I $(QHULLHDR) $^  -lm  -lgsl -lgslcblas $(QHULLIB)/libqhullstatic.a -o $@
KNTloc_linear.x: $(OBJS) KNTloc_linear.c
	$(CC) $(C_FLAGS)  -I $(DIR_HEADERS)    -I $(QHULLHDR) $^  -lm  -lgsl -lgslcblas $(QHULLIB)/libqhullstatic.a -o $@
KNTloc_ring.x: $(OBJS) KNTloc_ring.c
	$(CC) $(C_FLAGS)  -I $(DIR_HEADERS)    -I $(QHULLHDR) $^  -lm  -lgsl -lgslcblas $(QHULLIB)/libqhullstatic.a -o $@
KNTclose.x: $(OBJS) KNTclose.c
	$(CC) $(C_FLAGS)  -I $(DIR_HEADERS)    -I $(QHULLHDR) $^  -lm  -lgsl -lgslcblas $(QHULLIB)/libqhullstatic.a -o $@
# dependencies
KNT_lib.o :  KNT_lib.c  KNT_lib.h KNT_defaults.h $(OBJ1) $(OBJ2) $(OBJ3) $(OBJ4)
	$(CC) -c $(C_FLAGS) -I $(DIR_HEADERS)  -I $(QHULLHDR)   $<
KNT_closures.o :  KNT_closures.c  KNT_closures.h KNT_defaults.h $(OBJ1) $(OBJ2) $(OBJ3) KNT_qhull.o
	$(CC) -c $(C_FLAGS) -I $(DIR_HEADERS)  -I $(QHULLHDR)    $<
KNT_qhull.o :  KNT_qhull.c  KNT_qhull.h
	$(CC) -c $(C_FLAGS) -I $(DIR_HEADERS)  -I $(QHULLHDR)    $<
KNT_io.o :  KNT_io.c  KNT_io.h KNT_defaults.h $(OBJ3) $(OBJ1)
	$(CC) -c $(C_FLAGS) -I $(DIR_HEADERS)  $<
KNT_arc.o : KNT_arc.c KNT_arc.h KNT_defaults.h $(OBJ1)
	$(CC) -c $(C_FLAGS) -I $(DIR_HEADERS)  $<
Alexander.o : Alexander.c Alexander.h  $(OBJ1)
	$(CC) -c $(C_FLAGS)  -I $(DIR_HEADERS)   $<
my_random.o : my_random.c my_random.h
	$(CC) -c $(C_FLAGS)  $<
my_geom.o : my_geom.c  my_geom.h
	$(CC) -c $(C_FLAGS)  $<
my_memory.o : my_memory.c my_memory.h
	$(CC) -c $(C_FLAGS)  $<
messages.o : messages.c messages.h
	$(CC) -c $(C_FLAGS)  $<
$(QHULLIB)/libqhullstatic.a:
	cd $(QHULL); make
clean:
	rm *.o
deep-clean:
	rm *.o *.x
	cd $(QHULL); make cleanall
