#include "projections.h"

void q_frame_rotate(ref_frameT * rf)
{

  int l;
  double q[4], u[3];

  /* Asse rotazione */
  u[0]=cos(rf->q_phi) * sin(rf->q_theta);  
  u[1]=sin(rf->q_phi) * sin(rf->q_theta); 
  u[2]=cos(rf->q_theta);            

  /* quaternione */

  q[0]= cos(rf->q_psi/2.0);
  
  for( l = 0 ; l < 3; l++){
    q[l+1]=u[l]*sin(rf->q_psi/2);
  }

  if(rf != NULL)
  { 
    rf->ax[0][0]=q[0]*q[0]+q[1]*q[1]-q[2]*q[2]-q[3]*q[3];
    rf->ax[0][1]=2*(q[1]*q[2]-q[0]*q[3]);
    rf->ax[0][2]=2*(q[0]*q[2]+q[1]*q[3]);

    rf->ax[1][0]=2*(q[0]*q[3]+q[1]*q[2]);
    rf->ax[1][1]=q[0]*q[0]-q[1]*q[1]+q[2]*q[2]-q[3]*q[3];
    rf->ax[1][2]=2*(q[2]*q[3]-q[0]*q[1]);


    rf->ax[2][0]=2*(q[1]*q[3]-q[0]*q[2]);
    rf->ax[2][1]=2*(q[0]*q[1]+q[2]*q[3]);
    rf->ax[2][2]=q[0]*q[0]-q[1]*q[1]-q[2]*q[2]+q[3]*q[3];
  }

}

ref_frameT * q_init_frame()
{
  ref_frameT * rf;
  extern long int * ptr_idum;

  if((rf = malloc(sizeof(ref_frameT))) == NULL)
    failed("failed to allocate memory for reference frame\n");

  rf->next = NULL;

  /* angoli polari per l'asse di rotazione. Scelta uniforme */
  rf->q_phi    = ran2( ptr_idum ) * 2 * acos(-1.0);
  rf->q_theta  = acos( 2.0 * (ran2( ptr_idum ) -0.5));
  /* angolo di rotazione */
  rf->q_psi    = ran2( ptr_idum ) * 2 * acos(-1.0);

  rf->ax[0][0] = 1.0; rf->ax[0][1] = 0.0; rf->ax[0][2] = 0.0;
  rf->ax[1][0] = 0.0; rf->ax[1][1] = 1.0; rf->ax[1][2] = 0.0;
  rf->ax[2][0] = 0.0; rf->ax[2][1] = 0.0; rf->ax[2][2] = 1.0;

  q_frame_rotate( rf );

  return rf;

}

void add_frame(ref_frameT * rf)
{

  if(rf->next == NULL)
  {
    rf->next = q_init_frame( );
  }
  else
  {
    add_frame(rf->next);
  }

}

ref_frameT * get_frame ( ref_frameT * rf, int n)
{
  int i = 0;

  if( n == i )
  {
    return rf;
  }
  else if ( rf->next != NULL )
  {
    get_frame ( rf->next, n - 1);
  }
  else
  {
    failed("trying to access ref_frame out of bounds!\n");
  }
}

void free_ref_frameT(ref_frameT * rf)
{

  if(rf != NULL)
  { 
    free_ref_frameT ( rf->next );
    free ( rf );
  }

}

void  project_chain ( ref_frameT * rf, int N,  double ** chain, double ** prj_chain  )
{
  int i, j;

  for ( i = 0 ; i < N ; i++)
  {
    for ( j = 0; j < 3 ; j++)
    {
      prj_chain[i][j] = chain[i][0] * rf->ax[j][0] + chain[i][1] * rf->ax[j][1] + chain[i][2] * rf->ax[j][2];
    }
  }
}

