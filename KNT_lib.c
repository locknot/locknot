#include "KNT_lib.h"
#include "KNT_qhull.h"
#include "KNT_closures.h"

/*
 * GLOBAL VARIABLES
 *
 * Defining here the global variables used by the functions keeps
 * them hidden from the final user.
 */
double ** COORD_BUFFER = NULL;
double ** SIMP_BUFFER  = NULL;
int      stch_n_closures = 1000;       /* number of different closure (default)                                       */
int      stch_is_init    = FALSE;      /* not inizialized: default values used;                                       */
double   stch_treshold   = 0.5;        /* If less than stch_treshold closed arcs result in the same knot, the topology is left as uncertain */

extern int      qhull_is_init;        /* keep track of qhull_buffer memory state. Use it to avoid memory leaks       */
/*--------knot list -----------------*/
const KNTadets  knot_list[] = KNTadets_KNTLIST;

/*--------KNTarc global variables----*/

extern KNTarc KNTarc_DEFAULT;         /* defined in KNT_arc.c */

/*
 * KNTLknot_determination.
 *
 * Determine Alexander determinants and return knot_id
 * according to the table defined in KNT_defaults.h
 *
 * IN:
 *  KNTarc
 *  ALX_wspace: work space for alexander matrix determination.
 *   NULL if memory should be initialized and freed locally.
 * OUT:
 *  id of knot.
 */
int KNTLknot_determination ( KNTarc * knt_ptr, ALX_wspace * alx_wspace )
{
  int    N_simp_coord;
  double ** simp_coord;
  int    determinant;
  int    k_dets[2];
  int    t[2] = {-1,-2};
  int    is_alx_allocated = FALSE;

  /* check knt_ptr is correctly initialized */
  if( knt_ptr->is_init != 1 || knt_ptr->flag_len != 1 || knt_ptr->coord == NULL )
  {
    failed ( "KNTLknot_determination. knt_ptr is not correctly initialized!\n");
  }

  /*
   * check if Alexander determinants where already computed. If so,
   * simply determine the knot type (see TABLE in KNT_defaults.h)
   * and exit.
   */
  KNTarc_GETif(knt_ptr,Adet_1,k_dets[0],int);
  KNTarc_GETif(knt_ptr,Adet_2,k_dets[1],int);
  if( k_dets[0] != DONT_CARE && k_dets[1] != DONT_CARE )
  {
    return KNTLknot_id ( k_dets );
  }

  /* simplify chain prior to Alexander determinants computation */
  N_simp_coord = knt_ptr->len;
  if ( COORD_BUFFER == NULL )
  {
    simp_coord = d2t( N_simp_coord, 3 );
  }
  else
  {
    simp_coord = COORD_BUFFER;
  }
  chain_remove_beads_new (knt_ptr->len, knt_ptr->coord, &N_simp_coord, simp_coord, NULL, DONT_CARE);
  //chain_remove_beads_w_shuffle (knt_ptr->len, knt_ptr->coord,&N_simp_coord, simp_coord, DONT_CARE);
  //chain_remove_beads_w_shuffle_nostride (knt_ptr->len, knt_ptr->coord,&N_simp_coord, simp_coord);
  //solo per motivi di debug..
  //copy_chain(knt_ptr->coord,N_simp_coord,simp_coord);
  //DEBUG!!
  //fprintf(stderr,"--------------\n");
  //fprintf(stderr,"%d\n",N_simp_coord+1);
  //for(int i=0;i<N_simp_coord;i++)
  //{
  //  fprintf(stderr,"%lf %lf %lf\n",simp_coord[i][0],simp_coord[i][1],simp_coord[i][2]);
  //}
  //fprintf(stderr,"%lf %lf %lf\n",simp_coord[0][0],simp_coord[0][1],simp_coord[0][2]);
  //fprintf(stderr,"--------------\n");
  //fflush(stderr);
  //END DEBUG!!

  /* no knot can be formed with less than 5 segments */
  if( N_simp_coord < 4 )
  {
    k_dets[0] = 1;
    k_dets[1] = 1;
  }
  else
  {
    if(alx_wspace == NULL)
    {
      is_alx_allocated = TRUE ;
      alx_wspace = ALX_wspace_alloc   ( knt_ptr->len );
    }
    determinant = ALX_alex_generict ( N_simp_coord, simp_coord, 2, t, k_dets, alx_wspace);
  }
  KNTarc_SET_value(knt_ptr,Adet_1,k_dets[0],int);
  KNTarc_SET_value(knt_ptr,Adet_2,k_dets[1],int);

  //free allocated memory

  if ( COORD_BUFFER == NULL )
  {
    free_d2t(simp_coord);
  }

  if( is_alx_allocated )
  {
    ALX_free_wspace( alx_wspace );
  }

  return KNTLknot_id( k_dets );
}

/*
 * KNTLkntmatrix
 *
 * Compute all elements of knot matrix
 * using the closure scheme defined by *close_subchain.
 *
 * IN:
 *  KNTarc * knt_ptr, ring with coordinates etc.
 *  (*close_subchain) closure scheme.
 * OUT:
 *  KNTarc * knt_ptr
 *
 * Last changes: introduced a different loop for type linear. Only
 * half matrix  have to be computed in that case
 */
void KNTLkntmatrix ( KNTarc * knt_ptr, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace )
{
  int i, j, k, l, tmp_k_id, k_id;
  int start, end;
  int k_dets[2];
  int t[2] = {-1,-2};
  int is_random         = FALSE;    /* if a random closure scheme is adopted is_random = TRUE */
  int determinant       = DONT_CARE;
  int qhull_local_init  = FALSE;
  char str_cls[STR_OUT_MAXLEN] = "Undefined";
  int knot_table [KTABLE_LEN] = KTABLE;
  int knot_dist  [KTABLE_LEN+1];
  int max = 0;
  extern int stch_n_closures;
  extern double stch_treshold;

  KNTarc       arc = KNTarc_DEFAULT;

  /*check if structure is correctly initialized */

  if( knt_ptr->is_init != 1 || knt_ptr->flag_len != 1 || knt_ptr->coord == NULL )
  {
    failed ( "KNTLkntmatrix_qhull. knt_ptr is not correctly initialized!\n");
  }

  /*-----------------------------------------------------------------------*
   * Set flags according to the closure scheme adopted
   *-----------------------------------------------------------------------*/
  if( close_subchain == &KNTCqhull_close_subchain )
  {
    /*
     * if qhull is not initialized, initialize it.
     */
    if(!qhull_is_init)
    {
      qhull_init(knt_ptr->len*3);
      qhull_local_init = TRUE;
    }
    strncpy ( str_cls, QHULLCLOSURE, STR_OUT_MAXLEN );
  }
  else if( close_subchain == &KNTCqhull_hybrid_close_subchain )
  {
    if(!qhull_is_init)
    {
      qhull_init(knt_ptr->len*3);
      qhull_local_init = TRUE;
    }
    strncpy ( str_cls, QHULLCLOSUREHYB, STR_OUT_MAXLEN );
  }
  else if ( close_subchain == &KNTCrandom_close_subchain )
  {
    is_random = TRUE;
    strncpy ( str_cls,STCHCLOSURE, STR_OUT_MAXLEN );
  }
  else if ( close_subchain == &KNTCbridge_close_subchain )
  {
    strncpy ( str_cls,BRIDGECLOSURE,STR_OUT_MAXLEN );
  }
  /*-----------------------------------------------------------------------*/


  /*if matrix memory is not inizialized, append it to the structure */
  if( knt_ptr->kntmatrix == NULL )
  {
    KNTadd_matrix ( knt_ptr );
  }

  /* determine knot_id of  whole chain (see KNT_defaults.h) */
  KNTarc_GETif(knt_ptr,Adet_1,k_dets[0],int);
  KNTarc_GETif(knt_ptr,Adet_2,k_dets[1],int);
  if( k_dets[0] == DONT_CARE || k_dets[1] == DONT_CARE )
  {
    determinant = ALX_alex_generict ( knt_ptr->len, knt_ptr->coord, 2, t, k_dets, alx_wspace);
    KNTarc_SET_value(knt_ptr, Adet_1,k_dets[0],int);
    KNTarc_SET_value(knt_ptr, Adet_2,k_dets[1],int);
  }

  k_id = KNTLknot_id( k_dets );

  /*
   * Calculate all entries on knot_matrix. i is the beginning of
   * the arc, j its length ( including arc end )
   */
  if(knt_ptr->arc_type == ARC_ID_LIN )
  {
    for ( start = 0 ; start < knt_ptr->len ; start++ )
    {
      for ( end = start + 1 ; end < knt_ptr->len  ; end++ )
      {
        if( end - start < 4 )
        {
          knt_ptr->kntmatrix[start][ end ] = K_Un;
        }
        else if(knt_ptr->kntmatrix[start][ end ] == DONT_CARE)
        {
          if ( is_random == FALSE )
          {
            /* close the arc between start and end (included) */
            arc = ( *close_subchain ) ( knt_ptr, start, end );
            knt_ptr->kntmatrix[start][ end ] = KNTLknot_determination (&arc,  alx_wspace );
            /* free the coordinates inside structure arc */
            KNTfree_arc( &arc );
          }
          else
          {
            for ( l = 0 ; l <= KTABLE_LEN ; l++ )
            {
              knot_dist[l] = 0;
            }
            for ( k = 0 ; k < stch_n_closures ; k++ )
            {
              arc = ( *close_subchain ) ( knt_ptr, start, end );
              tmp_k_id = KNTLknot_determination (&arc,  alx_wspace );
              for ( l = 0 ; l < KTABLE_LEN ; l++ )
              {
                if ( tmp_k_id == knot_table[l] )
                {
                  knot_dist[l]++;
                  break;
                }
                /*
                 * I add one point to knot_dist, when no known knot
                 * is found, knot_dist last bin is incremented.
                 */
                if ( l == ( KTABLE_LEN - 1 ))
                {
                  knot_dist[KTABLE_LEN]++;
                  break;
                }
              }
              KNTfree_arc( &arc );
            }
            max = 0;
            for ( l = 1 ; l <= KTABLE_LEN ; l++ )
            {
              if ( knot_dist[l] > knot_dist[max] )
              {
                max = l ;
              }
            }
            if ( max < KTABLE_LEN && ( (double) knot_dist[max] / stch_n_closures) > stch_treshold )
            {
              knt_ptr->kntmatrix[start][end] = knot_table[max];
            }
            else if ( max == KTABLE_LEN )
            {
              knt_ptr->kntmatrix[start][end] = K_UNKNWN;
            }
            else
            {
              /*
               * IMPORTANT: in this case K_UNCRT collect both uncertain
               * topologies and Unknown knots!! This is a problem to be solved!
               */
              knt_ptr->kntmatrix[start][end] = K_UNCRT;
            }
          }
        }
        else
        {
          continue;
        }
      }
    }
  }
  else
  {
    for ( i = 0 ; i < knt_ptr->len ; i++ )
    {
      for ( j = 0 ; j < knt_ptr->len ; j++ )
      {
        start = i;
        end   = (i + j) % knt_ptr->len;
        if( j == 0 )
        {
          knt_ptr->kntmatrix[start][ end ] = k_id;
        }
        else if( j>0 && j < 4 )
        {
          knt_ptr->kntmatrix[start][ end ] = K_Un;
        }
        else if(knt_ptr->kntmatrix[start][ end ] == DONT_CARE)
        {
          if ( is_random == FALSE )
          {
            /* close the arc between start and end (included) */
            arc = ( *close_subchain ) ( knt_ptr, start, end );
            knt_ptr->kntmatrix[start][ end ] = KNTLknot_determination (&arc,  alx_wspace );
            /* free the coordinates inside structure arc */
            KNTfree_arc( &arc );
          }
          else
          {
            for ( l = 0 ; l <= KTABLE_LEN ; l++ )
            {
              knot_dist[l] = 0;
            }
            for ( k = 0 ; k < stch_n_closures ; k++ )
            {
              arc = ( *close_subchain ) ( knt_ptr, start, end );
              tmp_k_id = KNTLknot_determination (&arc,  alx_wspace );
              for ( l = 0 ; l < KTABLE_LEN ; l++ )
              {
                if ( tmp_k_id == knot_table[l] )
                {
                  knot_dist[l]++;
                  break;
                }
                /*
                 * I add one point to knot_dist, when no known knot
                 * is found, knot_dist last bin is incremented.
                 */
                if ( l == ( KTABLE_LEN - 1 ))
                {
                  knot_dist[KTABLE_LEN]++;
                  break;
                }
              }
              KNTfree_arc( &arc );
            }
            max = 0;
            for ( l = 1 ; l <= KTABLE_LEN ; l++ )
            {
              if ( knot_dist[l] > knot_dist[max] )
              {
                max = l ;
              }
            }
            if ( max < KTABLE_LEN && ( (double) knot_dist[max] / stch_n_closures) > stch_treshold )
            {
              knt_ptr->kntmatrix[start][end] = knot_table[max];
            }
            else if ( max == KTABLE_LEN )
            {
              knt_ptr->kntmatrix[start][end] = K_UNKNWN;
            }
            else
            {
              /*
               * IMPORTANT: in this case K_UNCRT collect both uncertain
               * topologies and Unknown knots!! This is a problem to be solved!
               */
              knt_ptr->kntmatrix[start][end] = K_UNCRT;
            }
          }
        }
        else
        {
          continue;
        }
      }
    }
  }
  /* set closure type identifier */
  strncpy(knt_ptr->closure,str_cls,strlen(str_cls));

  /* free memory */

  if(qhull_local_init)
  {
    qhull_terminate();
    qhull_is_init = 0;
  }
  else
  {
    qhull_free_mem();
  }
}

/*
 * KNTLloc_shrt_knot_unsafe.
 *
 * Localizes the shortest knotted arc(s) WITHOUT
 * checking that its complement chain is unknotted.
 *
 * The function returns a linked lists storing all such arcs.
 * They can be more than one for degenerated cases.
 *
 */

void KNTLloc_shrt_knot_unsafe ( KNTarc * knt_ptr,  KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace)
{
  int        i, j, k, l, tmp_k_id, k_id;
  int        start, end;
  int        last_cycle       = FALSE;
  int        qhull_local_init = FALSE;
  int        is_random = FALSE;
  int        knot_table [KTABLE_LEN] = KTABLE;
  int        knot_dist  [KTABLE_LEN+1];
  int        max = 0;
  char       str_cls[STR_OUT_MAXLEN] = "Undefined";
  KNTarc     tmp = KNTarc_DEFAULT;
  KNTarc     * knt_end;
  KNTarc     arc = KNTarc_DEFAULT;

  /*check if structure is correctly initialized */

  if( knt_ptr->is_init != 1 || knt_ptr->flag_len != 1 || knt_ptr->coord == NULL )
  {
    failed ( "KNTLloc_shrt_knot. knt_ptr is not correctly initialized!\n");
  }

  /* set the type of closure to be used according to input */
  /* set flags relative to closure                         */
  if ( close_subchain == &KNTCqhull_hybrid_close_subchain )
  {
    /*
     * if qhull is not initialized, initialize it.
     */

    if(!qhull_is_init)
    {
      qhull_init(knt_ptr->len*3);
      qhull_local_init = TRUE;
    }
    strncpy ( str_cls, QHULLCLOSUREHYB, STR_OUT_MAXLEN );
  }
  else if( close_subchain == &KNTCqhull_close_subchain )
  {
    if(!qhull_is_init)
    {
      qhull_init(knt_ptr->len*3);
      qhull_local_init = TRUE;
    }
    strncpy ( str_cls, QHULLCLOSURE, STR_OUT_MAXLEN );
  }
  else if ( close_subchain == &KNTCrandom_close_subchain )
  {
    is_random = TRUE;
    strncpy ( str_cls,STCHCLOSURE, STR_OUT_MAXLEN );
  }
  else if ( close_subchain == &KNTCbridge_close_subchain )
  {
    strncpy ( str_cls,BRIDGECLOSURE,STR_OUT_MAXLEN );
  }
  /*----------------------------------------------------------------*/

  /*if matrix memory is not inizialized, append it to the structure */
  if( knt_ptr->kntmatrix == NULL )
  {
    KNTadd_matrix ( knt_ptr );
  }

  /* determine knot_id of  whole chain (see KNT_defaults.h) */
  k_id = KNTLknot_determination( knt_ptr, alx_wspace );

  /* cycle on the knot matrix                               */
  for ( j = 1 ; j < knt_ptr->len ; j++ )
  {
    for ( i = 0 ; i < knt_ptr->len ; i++ )
    {
      start = i;
      end   = ( i + j ) % knt_ptr->len;
      /*
       * check the matrix to see if the topology of the arc
       * has already been determined. If not determine it.
       */
      if(knt_ptr->kntmatrix[start][end] == DONT_CARE)
      {
        if ( j == 0 ) /* in this case it's useless to check cmp_arc */
        {
          knt_ptr->kntmatrix[start][end] = k_id;
          continue;
        }
        else if ( j < 4 )
        {
          knt_ptr->kntmatrix[start][end] = K_Un;
        }
        else
        {
          if ( is_random == FALSE )
          {
            arc = ( *close_subchain )( knt_ptr , start, end );
            knt_ptr->kntmatrix[start][end] = KNTLknot_determination ( &arc, alx_wspace );
          }
          else
          {
            for ( l = 0 ; l <= KTABLE_LEN ; l++ )
            {
              knot_dist[l] = 0;
            }
            for ( k = 0 ; k < stch_n_closures ; k++ )
            {
              arc = ( *close_subchain ) ( knt_ptr, start, end );
              tmp_k_id = KNTLknot_determination (&arc,  alx_wspace );
              for ( l = 0 ; l < KTABLE_LEN ; l++ )
              {
                if ( tmp_k_id == knot_table[l] )
                {
                  knot_dist[l]++;
                  break;
                }
                /*
                 * I add one point to knot_dist, when no known knot
                 * is found, knot_dist last bin is incremented.
                 */
                if ( l == ( KTABLE_LEN - 1 ))
                {
                  knot_dist[KTABLE_LEN]++;
                  break;
                }
              }
            }
            max = 0;
            for ( l = 1 ; l <= KTABLE_LEN ; l++ )
            {
              if ( knot_dist[l] > knot_dist[max] )
              {
                max = l ;
              }
            }
            if ( max < KTABLE_LEN && ( (double) knot_dist[max] / stch_n_closures) > stch_treshold )
            {
              knt_ptr->kntmatrix[start][end] = knot_table[max];
            }
            else if ( max == KTABLE_LEN )
            {
              knt_ptr->kntmatrix[start][end] = K_UNKNWN;
            }
            else
            {
              knt_ptr->kntmatrix[start][end] = K_UNCRT;
            }
          }
        }
      }
      /*
       * If an arc with the same topology of the ring is found
       * save its properties attaching it to knt_ptr->next.
       */
      if(knt_ptr->kntmatrix[start][end] == k_id )
      {
        tmp = ( *close_subchain )( knt_ptr, start, end );
        knt_end = KNTpush ( knt_ptr, &tmp);
        /* I do not want to pass coordinates */
        free_d2t(knt_end->coord);
        knt_end->coord = NULL;
        KNTarc_SET_value(knt_end, arc_type, ARC_ID_SKU, char);
        KNTarc_SET_value(knt_end, Adet_1,knt_ptr->Adet_1,int);
        KNTarc_SET_value(knt_end, Adet_2,knt_ptr->Adet_2,int);
        strncpy(knt_end->closure,str_cls,strlen(str_cls));
        strncpy(knt_end->simplification,SIMPNONE,strlen(SIMPNONE));

        last_cycle = TRUE;
      }
      /* free coordinates in arc */
      KNTfree_arc ( &arc );
      /* reset arc */
      KNTreset    ( &arc     );
    }
    if ( last_cycle )
    {
      break;
    }
  }

  if(qhull_local_init)
  {
    qhull_terminate();
    qhull_is_init = 0;
  }
  else
  {
    qhull_free_mem();
  }
}

/*
 * KNTLloc_shrt_knot.
 *
 * Localizes the shortest knotted portion(s).
 * Shortest knotted portion: the shortest arc having
 * the same topology of the whole chain while its complement
 * is unknotted.
 * The function returns a linked lists storing all such arcs.
 * They can be more than one for degenerated cases.
 *
 */

void KNTLloc_shrt_knot ( KNTarc * knt_ptr,  KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace)
{
  int        i, j, k, l, tmp_k_id, k_id;
  int        start, end;
  int        last_cycle       = FALSE;
  int        qhull_local_init = FALSE;
  int        is_random = FALSE;
  int        knot_table [KTABLE_LEN] = KTABLE;
  int        knot_dist  [KTABLE_LEN+1];
  int        max = 0;
  char       str_cls[STR_OUT_MAXLEN] = "Undefined";
  KNTarc     * knt_end;
  KNTarc     tmp = KNTarc_DEFAULT;
  KNTarc     arc = KNTarc_DEFAULT;
  KNTarc     cmp_arc = KNTarc_DEFAULT;    /* arc and complementar arc */

  /*check if structure is correctly initialized */

  if( knt_ptr->is_init != 1 || knt_ptr->flag_len != 1 || knt_ptr->coord == NULL )
  {
    failed ( "KNTLloc_shrt_knot. knt_ptr is not correctly initialized!\n");
  }

  if ( close_subchain == &KNTCqhull_hybrid_close_subchain )
  {
    /*
     * if qhull is not initialized, initialize it.
     */

    if(!qhull_is_init)
    {
      qhull_init(knt_ptr->len*3);
      qhull_local_init = TRUE;
    }
    strncpy ( str_cls, QHULLCLOSUREHYB, STR_OUT_MAXLEN );
  }
  else if( close_subchain == &KNTCqhull_close_subchain )
  {
    if(!qhull_is_init)
    {
      qhull_init(knt_ptr->len*3);
      qhull_local_init = TRUE;
    }
    strncpy ( str_cls, QHULLCLOSURE, STR_OUT_MAXLEN );
  }
  else if ( close_subchain == &KNTCrandom_close_subchain )
  {
    is_random = TRUE;
    strncpy ( str_cls,STCHCLOSURE, STR_OUT_MAXLEN );
  }
  else if ( close_subchain == &KNTCbridge_close_subchain )
  {
    strncpy ( str_cls,BRIDGECLOSURE,STR_OUT_MAXLEN );
  }

  /*if matrix memory is not inizialized, append it to the structure */
  if( knt_ptr->kntmatrix == NULL )
  {
    KNTadd_matrix ( knt_ptr );
  }

  /* determine knot_id of  whole chain (see KNT_defaults.h) */
  k_id = KNTLknot_determination( knt_ptr, alx_wspace );

  for ( j = 0 ; j < knt_ptr->len ; j++ )
  {
    for ( i = 0 ; i < knt_ptr->len ; i++ )
    {
      start = i;
      end   = ( i + j ) % knt_ptr->len;
      /*
       * check the matrix to see if the topology of the two arcs
       * has already been determined. If not do it.
       */
      /* principal arc */
      if(knt_ptr->kntmatrix[start][end] == DONT_CARE)
      {
        if ( j == 0 ) /* in this case it's useless to check cmp_arc */
        {
          knt_ptr->kntmatrix[start][end] = k_id;
          continue;
        }
        else if ( j < 4 )
        {
          knt_ptr->kntmatrix[start][end] = K_Un;
        }
        else
        {
          if ( is_random == FALSE )
          {
            arc = ( *close_subchain )( knt_ptr , start, end );
            knt_ptr->kntmatrix[start][end] = KNTLknot_determination ( &arc, alx_wspace );
          }
          else
          {
            for ( l = 0 ; l <= KTABLE_LEN ; l++ )
            {
              knot_dist[l] = 0;
            }
            for ( k = 0 ; k < stch_n_closures ; k++ )
            {
              arc = ( *close_subchain ) ( knt_ptr, start, end );
              tmp_k_id = KNTLknot_determination (&arc,  alx_wspace );
              for ( l = 0 ; l < KTABLE_LEN ; l++ )
              {
                if ( tmp_k_id == knot_table[l] )
                {
                  knot_dist[l]++;
                  break;
                }
                /*
                 * I add one point to knot_dist, when no known knot
                 * is found, knot_dist last bin is incremented.
                 */
                if ( l == ( KTABLE_LEN - 1 ))
                {
                  knot_dist[KTABLE_LEN]++;
                  break;
                }
              }
            }
            max = 0;
            for ( l = 1 ; l <= KTABLE_LEN ; l++ )
            {
              if ( knot_dist[l] > knot_dist[max] )
              {
                max = l ;
              }
            }
            if ( max < KTABLE_LEN && ( (double) knot_dist[max] / stch_n_closures) > stch_treshold )
            {
              knt_ptr->kntmatrix[start][end] = knot_table[max];
            }
            else if ( max == KTABLE_LEN )
            {
              knt_ptr->kntmatrix[start][end] = K_UNKNWN;
            }
            else
            {
              /*
               * IMPORTANT: in this case K_UNCRT collect both uncertain
               * topologies and Unknown knots!! This is a problem to be solved!
               */
              knt_ptr->kntmatrix[start][end] = K_UNCRT;
            }
          }
        }
      }
      /* complementar arc */
      if(knt_ptr->kntmatrix[end][start] == DONT_CARE)
      {
        if( ( knt_ptr->len - j ) < 4 )
        {
          knt_ptr->kntmatrix[end][start] = K_Un;
        }
        else
        {
          if ( is_random == FALSE )
          {
            cmp_arc = ( *close_subchain )( knt_ptr , end, start );
            knt_ptr->kntmatrix[end][start] = KNTLknot_determination ( &cmp_arc, alx_wspace );
          }
          else
          {
            for ( l = 0 ; l <= KTABLE_LEN ; l++ )
            {
              knot_dist[l] = 0;
            }
            for ( k = 0 ; k < stch_n_closures ; k++ )
            {
              cmp_arc = ( *close_subchain ) ( knt_ptr, start, end );
              tmp_k_id = KNTLknot_determination (&cmp_arc,  alx_wspace );
              for ( l = 0 ; l < KTABLE_LEN ; l++ )
              {
                if ( tmp_k_id == knot_table[l] )
                {
                  knot_dist[l]++;
                  break;
                }
                /*
                 * I add one point to knot_dist, when no known knot
                 * is found, knot_dist last bin is incremented.
                 */
                if ( l == ( KTABLE_LEN - 1 ))
                {
                  knot_dist[KTABLE_LEN]++;
                  break;
                }
              }
            }
            max = 0;
            for ( l = 1 ; l <= KTABLE_LEN ; l++ )
            {
              if ( knot_dist[l] > knot_dist[max] )
              {
                max = l ;
              }
            }
            if ( max < KTABLE_LEN && ( (double) knot_dist[max] / stch_n_closures) > stch_treshold )
            {
              knt_ptr->kntmatrix[end][start] = knot_table[max];
            }
            else if ( max == KTABLE_LEN )
            {
              knt_ptr->kntmatrix[end][start] = K_UNKNWN;
            }
            else
            {
              /*
               * IMPORTANT: in this case K_UNCRT collect both uncertain
               * topologies and Unknown knots!! This is a problem to be solved!
               */
              knt_ptr->kntmatrix[end][start] = K_UNCRT;
            }
          }
        }
      }
      /*
       * If a stable arc (s.t. arc(i,j) has topology k_id, while arc(j,i) is unknot )
       * save its properties attaching it to knt_ptr->next.
       */
      if(knt_ptr->kntmatrix[start][end] == k_id && knt_ptr->kntmatrix[end][start] == K_Un )
      {
        tmp = ( *close_subchain )( knt_ptr, start, end );
        knt_end = KNTpush ( knt_ptr, &tmp);
        /* I do not want to pass coordinates */
        free_d2t(knt_end->coord);
        knt_end->coord = NULL;
        KNTarc_SET_value(knt_end, arc_type, ARC_ID_SK, char);
        KNTarc_SET_value(knt_end, Adet_1,knt_ptr->Adet_1,int);
        KNTarc_SET_value(knt_end, Adet_2,knt_ptr->Adet_2,int);
        strncpy(knt_end->closure,str_cls,strlen(str_cls));
        strncpy(knt_end->simplification,SIMPNONE,strlen(SIMPNONE));

        last_cycle = TRUE;
      }
      /* free coordinates in arc and cmp_arc */
      KNTfree_arc ( &arc );
      KNTfree_arc ( &cmp_arc );
      /* reset arc and cmp_arc */
      KNTreset    ( &arc     );
      KNTreset    ( &cmp_arc );
    }
    if ( last_cycle )
    {
      break;
    }
  }

  if(qhull_local_init)
  {
    qhull_terminate();
    qhull_is_init = 0;
  }
  else
  {
    qhull_free_mem();
  }
}

/*
 * KNTLloc_shrtcnt_knot.
 *
 * Localizes the shortest continuosly knotted portion(s).
 *
 * The shortest C-knotted portion is the shortest arc in
 * a succession of enclosed arcs having the topology of the ring
 * and unknotted complement.
 *
 * The function returns a linked lists storing all such arcs.
 * They can be more than one for degenerated cases.
 *
 */
void KNTLloc_shrtcnt_knot ( KNTarc * knt_ptr, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace  )
{
  int        i, j, k_id;
  int        start, end, klen;
  int        _continue_       = FALSE;
  int        qhull_local_init = FALSE;
  char       str_cls[STR_OUT_MAXLEN] = "Undefined";
  KNTarc     arc = KNTarc_DEFAULT;
  KNTarc     cmp_arc = KNTarc_DEFAULT;    /* arc and complementar arc */
  KNTarc     tmp  = KNTarc_DEFAULT;
  KNTarc     *  knt_end;
  short      ** f_matrix;

  /*check if structure is correctly initialized */

  if( knt_ptr->is_init != 1 || knt_ptr->flag_len != 1 || knt_ptr->coord == NULL )
  {
    failed ( "KNTLloc_shrtcnt_knot. knt_ptr is not correctly initialized!\n");
  }

  if ( close_subchain == &KNTCqhull_hybrid_close_subchain )
  {
    /*
     * if qhull is not initialized, initialize it.
     */

    if(!qhull_is_init)
    {
      qhull_init(knt_ptr->len*3);
      qhull_local_init = TRUE;
    }
    strncpy ( str_cls,QHULLCLOSUREHYB, STR_OUT_MAXLEN);
  }

  /*if matrix memory is not inizialized, append it to the structure */
  if( knt_ptr->kntmatrix == NULL )
  {
    KNTadd_matrix ( knt_ptr );
  }

  /* determine knot_id of  whole chain (see KNT_defaults.h) */
  k_id = KNTLknot_determination ( knt_ptr, alx_wspace );

  /*
   * Initialize f_matrix f = 1, 0, -1.
   * Initialize diagonal elements of knot_matrix.
   */
  f_matrix = s2t(knt_ptr->len, knt_ptr->len);

  for ( i = 0 ; i < knt_ptr->len; i++ )
  {
    f_matrix[i][ knt_ptr->len - 1 ] = 1;
    if ( knt_ptr->kntmatrix[i][i] == DONT_CARE )
    {
      knt_ptr->kntmatrix[i][i] = k_id;
    }
    for ( j = knt_ptr->len - 2 ; j > 0 ; j-- )
    {
      f_matrix[i][j] = -1;
    }
  }

  klen = knt_ptr->len;
  for ( j = knt_ptr->len - 1 ; j > 3 ; j-- )
  {
    _continue_ = FALSE;
    for ( i = 0 ; i < knt_ptr->len ; i++ )
    {
      /*
       * The following condition guarantees that there exists
       * a continuos succession of stable knotted arc connecting next point
       * to the diagonal of the knot matrix
       *
       * C arrays begin with 0. So the element of matrix [i][j] refers to the
       * arc starting in i of length j+1 ( including start and end ).
       */
      if( (f_matrix[i][ j ] == TRUE ) || (f_matrix[ ( i - 1 + knt_ptr->len )%knt_ptr->len][j] == TRUE ) )
      {
        start = i;
        end   = ( i + j ) % knt_ptr->len;
        /*
         * check the matrix to see if the topology of the two arcs
         * has already been determined. If not do it.
         */
        if(knt_ptr->kntmatrix[start][end] == DONT_CARE)
        {
          if( j < 4 ) /*in this case no knots can be formed */
          {
            knt_ptr->kntmatrix[start][end] = K_Un;
          }
          else
          {
            arc = ( *close_subchain )( knt_ptr , start, end );
            knt_ptr->kntmatrix[start][end] = KNTLknot_determination ( &arc, alx_wspace );
          }

        }
        if(knt_ptr->kntmatrix[end][start] == DONT_CARE)
        {
          if ( (knt_ptr->len - j)  < 4 ) /* no knots possible */
          {
            knt_ptr->kntmatrix[end][start] = K_Un;
          }
          else
          {
            cmp_arc = ( *close_subchain )( knt_ptr , end, start );
            knt_ptr->kntmatrix[end][start] = KNTLknot_determination ( &cmp_arc, alx_wspace );
          }
        }
        /*
         * If a stable arc set f_matrix to TRUE and flag _continue_ to true.
         */
        if(knt_ptr->kntmatrix[start][end] == k_id && knt_ptr->kntmatrix[end][start] == K_Un )
        {
          klen           = j;
          f_matrix[i][j-1] = TRUE;
          _continue_     = TRUE;
        }
        else
        {
          f_matrix[i][j-1] = FALSE;
        }
      }
      /* free coordinates in arc and cmp_arc */
      KNTfree_arc ( &arc     );
      KNTfree_arc ( &cmp_arc );
      /* reset arc and cmp_arc */
      KNTreset    ( &arc     );
      KNTreset    ( &cmp_arc );
    }
    if ( !_continue_ )
    {
      break;
    }
  }
  /*
   * reread last useful line and save the properties
   * of the shortest continuosly knotted portion
   * encountered.
   *
   */
  for( i = 0; i < knt_ptr->len; i++)
  {
    if( f_matrix[i][klen-1] == TRUE )
    {
      start = i;
      end   = ( i + klen ) % knt_ptr->len;

      tmp = ( *close_subchain )( knt_ptr, start, end );
      knt_end =KNTpush ( knt_ptr, &tmp);
      free_d2t(knt_end->coord);
      knt_end->coord = NULL;

      KNTarc_SET_value(knt_end, arc_type, ARC_ID_SCK, char);
      KNTarc_SET_value(knt_end, Adet_1,knt_ptr->Adet_1,int);
      KNTarc_SET_value(knt_end, Adet_2,knt_ptr->Adet_2,int);
      strncpy(knt_end->closure,str_cls,strlen(str_cls));
      strncpy(knt_end->simplification,SIMPNONE,strlen(SIMPNONE));

    }
  }
#if DEBUG == 2
  for ( j = knt_ptr->len - 1 ; j > 0 ; j-- )
  {
    for ( i = 0 ; i < knt_ptr->len; i++ )
    {
      fprintf(stderr,"%4d %4d %2d\n ",i,j,f_matrix[i][j]);
    }
  }
#endif

  free_s2t ( f_matrix );

  if(qhull_local_init)
  {
    qhull_terminate();
    qhull_is_init = 0;
  }
  else
  {
    qhull_free_mem();
  }
}


/*
 * KNTLloc_shrt_knot_rect
 *
 * Performs the same operations done by
 * KNTLloc_shrt_knot, but uses chain rectification
 * to cancel the unimportant points.
 *
 * Topologies are computed on the original, unsimplified, chain.
 *
 */
void KNTLloc_shrt_knot_rect ( KNTarc * knt_ptr, KNTarc * knt_rect, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace )
{
  int          i, j, k,  start, end;
  int          k_id;
  short      * f_vector;
  int          qhull_local_init = FALSE;
  char         str_cls[STR_OUT_MAXLEN] = "Undefined";
  int          last_cycle = FALSE;
  KNTarc     * knt_end;
  KNTarc       arc     = KNTarc_DEFAULT;
  KNTarc       cmp_arc = KNTarc_DEFAULT;
  KNTarc       tmp     = KNTarc_DEFAULT;
//  extern long int * ptr_idum;

  /*check if structure is correctly initialized */

  if( knt_ptr->is_init != 1 || knt_ptr->flag_len != 1 || knt_ptr->coord == NULL )
  {
    failed ( "KNTLloc_shrt_knot_rect. knt_ptr is not correctly initialized!\n");
  }

  if ( close_subchain == &KNTCqhull_hybrid_close_subchain )
  {
    /*
     * if qhull is not initialized, initialize it.
     */

    if(!qhull_is_init)
    {
      qhull_init(knt_ptr->len*3);
      qhull_local_init = TRUE;
    }
    strncpy ( str_cls,QHULLCLOSUREHYB, STR_OUT_MAXLEN);
  }

  /*if knt_ptr->kntmatrix memory is not inizialized, append it to the structure */
  if( knt_ptr->kntmatrix == NULL )
  {
    KNTadd_matrix ( knt_ptr );
  }

  /* determine knot_id of  whole chain (see KNT_defaults.h) */
  k_id = KNTLknot_determination( knt_ptr, alx_wspace );

  f_vector = s1t( knt_ptr->len );


  for( i = 0 ; i < knt_ptr->len ; i++ )
  {
    f_vector[i] = FALSE;
    for ( j = 0 ; j < knt_rect->len ; j++ )
    {
      if ( i == knt_rect->index[j] )
      {
        f_vector[i] = TRUE;
        break;
      }
    }
  }

  for ( j = 0 ; j < knt_ptr->len ; j++ )
  {
    for ( i = 0 ; i < knt_ptr->len ; i++ )
    {
      start = i;
      end   = ( i + j ) % knt_ptr->len;
      if( f_vector[start] == TRUE && f_vector[end] == TRUE)
      {

        if(knt_ptr->kntmatrix[start][end] == DONT_CARE)
        {
          if ( j == 0 ) /* in this case it's useless to check cmp_arc */
          {
            knt_ptr->kntmatrix[start][end] = k_id;
            continue;
          }
          else if ( j < 4 )
          {
            knt_ptr->kntmatrix[start][end] = K_Un;
          }
          else
          {
            arc = ( *close_subchain )( knt_ptr , start, end );
            knt_ptr->kntmatrix[start][end] = KNTLknot_determination ( &arc, alx_wspace );
            /* free coordinates in arc and cmp_arc */
            KNTfree_arc ( &arc );
            /* reset arc and cmp_arc */
            KNTreset    ( &arc     );
          }
        }
        /* complementar arc */
        if(knt_ptr->kntmatrix[end][start] == DONT_CARE)
        {
          if( ( knt_ptr->len - j ) < 4 )
          {
            knt_ptr->kntmatrix[end][start] = K_Un;
          }
          else
          {
            cmp_arc = ( *close_subchain )( knt_ptr , end, start );
            knt_ptr->kntmatrix[end][start] = KNTLknot_determination ( &cmp_arc, alx_wspace );
            /* free coordinates in arc and cmp_arc */
            KNTfree_arc ( &cmp_arc );
            /* reset arc and cmp_arc */
            KNTreset    ( &cmp_arc     );
          }
        }
        /*
         * If a stable arc (s.t. arc(i,j) has topology k_id, while arc(j,i) is unknot )
         * save its properties attaching it to knt_ptr->next.
         */
        if(knt_ptr->kntmatrix[start][end] == k_id && knt_ptr->kntmatrix[end][start] == K_Un )
        {
          tmp = ( *close_subchain )( knt_ptr, start, end );
          knt_end = KNTpush ( knt_ptr, &tmp);
          /*
           * exchange the indeces of simplified ring with those of saved ring.
           * This is a terrible piece of code which should be changed asap.
           */
          if( knt_end->index == NULL )
          {
            knt_end->index = i1t(knt_rect->len);
            KNTarc_SET_value(knt_end, len,knt_rect->len,int);
          }
          else
          {
            free_i1t ( knt_end->index );
            knt_end->index = i1t(knt_rect->len);
            KNTarc_SET_value(knt_end, len,knt_rect->len,int);
          }
          for ( k = 0 ; k < knt_rect->len; k++)
            knt_end->index[k] = knt_rect->index[k];

          /* I do not want to pass coordinates */
          //free_d2t(knt_end ->coord);
          //knt_end ->coord = NULL;
          KNTarc_SET_value(knt_end , arc_type, ARC_ID_SK, char);
          KNTarc_SET_value(knt_end , Adet_1,knt_ptr->Adet_1,int);
          KNTarc_SET_value(knt_end , Adet_2,knt_ptr->Adet_2,int);
          strncpy(knt_end ->closure,str_cls,strlen(str_cls));
          strncpy(knt_end ->simplification,SIMPRECT,strlen(SIMPRECT));
          last_cycle = TRUE;
        }
      }
    }
    if ( last_cycle )
    {
      break;
    }
  }
  /* free memory */
  free_s1t ( f_vector );

}

void KNTLloc_shrt_knot_unsafe_rect ( KNTarc * knt_ptr, KNTarc * knt_rect, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace )
{
  int          i, j, k,  start, end;
  int          k_id;
  short      * f_vector;
  int          qhull_local_init = FALSE;
  char         str_cls[STR_OUT_MAXLEN] = "Undefined";
  int          last_cycle = FALSE;
  KNTarc     * knt_end;
  KNTarc       arc     = KNTarc_DEFAULT;
  KNTarc       tmp     = KNTarc_DEFAULT;
//  extern long int * ptr_idum;

  /*check if structure is correctly initialized */

  if( knt_ptr->is_init != 1 || knt_ptr->flag_len != 1 || knt_ptr->coord == NULL )
  {
    failed ( "KNTLloc_shrt_knot_rect. knt_ptr is not correctly initialized!\n");
  }

  if ( close_subchain == &KNTCqhull_hybrid_close_subchain )
  {
    /*
     * if qhull is not initialized, initialize it.
     */

    if(!qhull_is_init)
    {
      qhull_init(knt_ptr->len*3);
      qhull_local_init = TRUE;
    }
    strncpy ( str_cls,QHULLCLOSUREHYB, STR_OUT_MAXLEN);
  }

  /*if knt_ptr->kntmatrix memory is not inizialized, append it to the structure */
  if( knt_ptr->kntmatrix == NULL )
  {
    KNTadd_matrix ( knt_ptr );
  }

  /* determine knot_id of  whole chain (see KNT_defaults.h) */
  k_id = KNTLknot_determination( knt_ptr, alx_wspace );

  f_vector = s1t( knt_ptr->len );


  for( i = 0 ; i < knt_ptr->len ; i++ )
  {
    f_vector[i] = FALSE;
    for ( j = 0 ; j < knt_rect->len ; j++ )
    {
      if ( i == knt_rect->index[j] )
      {
        f_vector[i] = TRUE;
        break;
      }
    }
  }

  for ( j = 0 ; j < knt_ptr->len ; j++ )
  {
    for ( i = 0 ; i < knt_ptr->len ; i++ )
    {
      start = i;
      end   = ( i + j ) % knt_ptr->len;
      if( f_vector[start] == TRUE && f_vector[end] == TRUE)
      {

        if(knt_ptr->kntmatrix[start][end] == DONT_CARE)
        {
          if ( j == 0 ) /* in this case it's useless to check cmp_arc */
          {
            knt_ptr->kntmatrix[start][end] = k_id;
            continue;
          }
          else if ( j < 4 )
          {
            knt_ptr->kntmatrix[start][end] = K_Un;
          }
          else
          {
            arc = ( *close_subchain )( knt_ptr , start, end );
            knt_ptr->kntmatrix[start][end] = KNTLknot_determination ( &arc, alx_wspace );
            /* free coordinates in arc and cmp_arc */
            KNTfree_arc ( &arc );
            /* reset arc and cmp_arc */
            KNTreset    ( &arc     );
          }
        }
        /*
         * If a stable arc (s.t. arc(i,j) has topology k_id
				 * save its properties attaching it to knt_ptr->next.
         */
        if(knt_ptr->kntmatrix[start][end] == k_id )
        {
          tmp = ( *close_subchain )( knt_ptr, start, end );
          knt_end = KNTpush ( knt_ptr, &tmp);
          /*
           * exchange the indeces of simplified ring with those of saved ring.
           * This is a terrible piece of code which should be changed asap.
           */
          if( knt_end->index == NULL )
          {
            knt_end->index = i1t(knt_rect->len);
            KNTarc_SET_value(knt_end, len,knt_rect->len,int);
          }
          else
          {
            free_i1t ( knt_end->index );
            knt_end->index = i1t(knt_rect->len);
            KNTarc_SET_value(knt_end, len,knt_rect->len,int);
          }
          for ( k = 0 ; k < knt_rect->len; k++)
            knt_end->index[k] = knt_rect->index[k];

          /* I do not want to pass coordinates */
          //free_d2t(knt_end ->coord);
          //knt_end ->coord = NULL;
          KNTarc_SET_value(knt_end , arc_type, ARC_ID_SKU, char);
          KNTarc_SET_value(knt_end , Adet_1,knt_ptr->Adet_1,int);
          KNTarc_SET_value(knt_end , Adet_2,knt_ptr->Adet_2,int);
          strncpy(knt_end ->closure,str_cls,strlen(str_cls));
          strncpy(knt_end ->simplification,SIMPRECT,strlen(SIMPRECT));
          last_cycle = TRUE;
        }
      }
    }
    if ( last_cycle )
    {
      break;
    }
  }
  /* free memory */
  free_s1t ( f_vector );

}

void KNTLloc_shrtcnt_knot_rect ( KNTarc * knt_ptr, KNTarc * knt_rect, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace)
{
  int        i, j, k_id;
  int        start, end, klen, klen_good, klen_tmp;
  int        start_good, end_good;
  int        _continue_       = FALSE;
  int        qhull_local_init = FALSE;
  char       str_cls[STR_OUT_MAXLEN] = "Undefined";
  KNTarc     arc = KNTarc_DEFAULT;
  KNTarc     tmp     = KNTarc_DEFAULT;
  KNTarc     cmp_arc = KNTarc_DEFAULT;    /* arc and complementar arc */
  KNTarc     *  knt_end;
  short      ** f_matrix;

  /*check if structure is correctly initialized */

  if( knt_ptr->is_init != 1 || knt_ptr->flag_len != 1 || knt_ptr->coord == NULL )
  {
    failed ( "KNTLloc_shrtcnt_knot. knt_ptr is not correctly initialized!\n");
  }

  if ( close_subchain == &KNTCqhull_hybrid_close_subchain )
  {
    /*
     * if qhull is not initialized, initialize it.
     */

    if(!qhull_is_init)
    {
      qhull_init(knt_ptr->len*3);
      qhull_local_init = TRUE;
    }
    strncpy ( str_cls,QHULLCLOSUREHYB, STR_OUT_MAXLEN);
  }

  /*if matrix memory is not inizialized, append it to the structure */
  if( knt_ptr->kntmatrix == NULL )
  {
    KNTadd_matrix ( knt_ptr );
  }

  /* determine knot_id of  whole chain (see KNT_defaults.h) */
  k_id = KNTLknot_determination ( knt_ptr, alx_wspace );

  /*
   * Initialize f_matrix f = 1, 0, -1.
   * Initialize diagonal elements of knot_matrix.
   */
  f_matrix = s2t(knt_rect->len, knt_rect->len);

  for ( i = 0 ; i < knt_rect->len; i++ )
  {
    f_matrix[i][ knt_rect->len - 1 ] = 1;
    /*
     * the following is a stronger assumption: If I was able to rectify
     * the chain between points i and i+1, the I haven't changed the
     * topology, and I can take it as granted without checking..
     */
  //  f_matrix[i][ knt_rect->len - 2 ] = 1;
    /*if ( knt_ptr->kntmatrix[i][i] == DONT_CARE )
    {
      knt_ptr->kntmatrix[i][i] = k_id;
    }*/
    for ( j = knt_rect->len - 2 ; j > 0 ; j-- )
    {
      f_matrix[i][j] = -1;
    }
  }

  klen = knt_ptr->len;
  for ( j = knt_rect->len - 1 ; j > 3 ; j-- )
  {
    _continue_ = FALSE;
    for ( i = 0 ; i < knt_rect->len ; i++ )
    {
      /*
       * The following condition guarantees that there exists
       * a continuos succession of stable knotted arc connecting next point
       * to the diagonal of the knot matrix
       *
       * C arrays begin with 0. So the element of matrix [i][j] refers to the
       * arc starting in i of length j+1 ( including start and end ).
       */
      if( (f_matrix[i][ j ] == TRUE ) || (f_matrix[ ( i - 1 + knt_rect->len )%knt_rect->len][j] == TRUE ) )
      {
        start = knt_rect->index[i];
        end   = ( i + j ) % knt_rect->len;
        end   = knt_rect->index[end];
        /*
         * check the matrix to see if the topology of the two arcs
         * has already been determined. If not do it.
         */
        if(knt_ptr->kntmatrix[start][end] == DONT_CARE)
        {
          arc = ( *close_subchain )( knt_ptr , start, end );
          knt_ptr->kntmatrix[start][end] = KNTLknot_determination ( &arc, alx_wspace );
        }
        if(knt_ptr->kntmatrix[end][start] == DONT_CARE)
        {
          if ( (knt_rect->len - j)  < 4 ) // no knots possible
          {
            knt_ptr->kntmatrix[end][start] = K_Un;
          }
          else
          {
            cmp_arc = ( *close_subchain )( knt_ptr , end, start );
            knt_ptr->kntmatrix[end][start] = KNTLknot_determination ( &cmp_arc, alx_wspace );
          }
        }
        /*
         * If a stable arc set f_matrix to TRUE and flag _continue_ to true.
         */
        if(knt_ptr->kntmatrix[start][end] == k_id && knt_ptr->kntmatrix[end][start] == K_Un )
        {
          /*doppio uso di start, end e klen! qui si intendono sulla catena semplificata!*/
          start = i;
          end   = ( i + j ) % knt_rect->len;
          klen           = j;
          f_matrix[i][j-1] = TRUE;
          _continue_     = TRUE;
        }
        else
        {
          f_matrix[i][j-1] = FALSE;
        }
      }
      /* free coordinates in arc and cmp_arc */
      KNTfree_arc ( &arc     );
      KNTfree_arc ( &cmp_arc );
      /* reset arc and cmp_arc */
      KNTreset    ( &arc     );
      KNTreset    ( &cmp_arc );
    }
    if ( !_continue_ )
    {
      break;
    }
  }
  /*
   * reread last useful line and save the properties
   * of the shortest continuosly knotted portion
   * encountered.
   *
   */
  klen_good = knt_ptr->len;
  start_good = 0;
  end_good = knt_ptr->len;
  for ( j = knt_rect->len - 1 ; j >= klen ; j-- )
  {
    for ( i = 0 ; i < knt_rect->len ; i++ )
    {
      start = knt_rect->index[i];
      end   = ( i + j ) % knt_rect->len;
      end   = knt_rect->index[end];
      klen_tmp = ( end > start ) ? end - start : end + knt_ptr->len - start;
      if( klen_tmp < klen_good  && f_matrix[i][j-1] == TRUE )
      {
        klen_good = klen_tmp;
        start_good = start;
        end_good   = end;
        /*------*/
        KNTfree_arc ( &arc );
        KNTreset    ( &arc );

        KNTarc_SET_value(&arc, start,start_good,int);
        KNTarc_SET_value(&arc, end,end_good,int);
        KNTarc_SET_value(&arc, arc_type, ARC_ID_SCK, char);
        KNTarc_SET_value(&arc, Adet_1,knt_ptr->Adet_1,int);
        KNTarc_SET_value(&arc, Adet_2,knt_ptr->Adet_2,int);
      }
    }
  }
#if DEBUG == 2
  for ( j = knt_rect->len - 1 ; j > 0 ; j-- )
  {
    for ( i = 0 ; i < knt_rect->len; i++ )
    {
      fprintf(stderr,"%4d %4d %2d\n ",i,j,f_matrix[i][j]);
    }
  }
#endif
  /*copy everything on knt_end */
  tmp = ( *close_subchain )( knt_ptr, start_good, end_good );
  knt_end = KNTpush ( knt_ptr, &tmp);
  KNTarc_SET_value(knt_end, arc_type, ARC_ID_SCK, char);
  KNTarc_SET_value(knt_end, Adet_1,knt_ptr->Adet_1,int);
  KNTarc_SET_value(knt_end, Adet_2,knt_ptr->Adet_2,int);
  strncpy(knt_end->closure,str_cls,strlen(str_cls));
  strncpy(knt_end->simplification,SIMPRECT,strlen(SIMPRECT));
  /*
   * exchange the indeces of simplified ring with those of saved ring.
   * This is a terrible piece of code which should be changed asap.
   */
  if( knt_end->index == NULL )
  {
    knt_end->index = i1t(knt_rect->len);
    KNTarc_SET_value(knt_end, len,knt_rect->len,int);
  }
  else
  {
    free_i1t ( knt_end->index );
    knt_end->index = i1t(knt_rect->len);
    KNTarc_SET_value(knt_end, len,knt_rect->len,int);
  }

  for ( i = 0 ; i < knt_rect->len; i++)
  {
    knt_end->index[i] = knt_rect->index[i];
  }
  if(knt_end->coord != NULL )
  {
    free_d2t(knt_end->coord);
    knt_end->coord = NULL;
  }

  free_s2t ( f_matrix );
if(qhull_local_init)
  {
    qhull_terminate();
    qhull_is_init = 0;
  }
  else
  {
    qhull_free_mem();
  }
}

void KNTLloc_shrtcnt_knot_rect1 ( KNTarc * knt_ptr, KNTarc * knt_rect, int st_p, int end_p, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace)
{
  int        i, j, k, l, k_id;
  int        start, end, klen, knot_len;
  int        r_start, r_end;
  int        _continue_       = FALSE;
  int        qhull_local_init = FALSE;
  char       str_cls[STR_OUT_MAXLEN] = "Undefined";
  char       fail_str[128];
  KNTarc     arc = KNTarc_DEFAULT;
  KNTarc     cmp_arc = KNTarc_DEFAULT;    /* arc and complementar arc */
  KNTarc       tmp     = KNTarc_DEFAULT;
  KNTarc     *  knt_end;
  short      ** f_matrix;
  int        arcs_array_len;
  KNTarc     * arcs_array;

  /*-----STARTING  routine checks ----------------------------------*/
  /*check if structure is correctly initialized */

  if( knt_ptr->is_init != 1 || knt_ptr->flag_len != 1 || knt_ptr->coord == NULL )
  {
    failed ( "KNTLloc_shrtcnt_knot. knt_ptr is not correctly initialized!\n");
  }
  if( knt_rect->is_init != 1 || knt_rect->flag_len != 1 || knt_rect->coord == NULL )
  {
    failed ( "KNTLloc_shrtcnt_knot. knt_rect is not correctly initialized!\n");
  }
  /* check closure and inizialize qhull if needed */
  if ( close_subchain == &KNTCqhull_hybrid_close_subchain  )
  {
    /*
     * if qhull is not initialized, initialize it.
     */
    if(!qhull_is_init)
    {
      qhull_init(knt_rect->len*3);
      qhull_local_init = TRUE;
    }
    strncpy ( str_cls,QHULLCLOSUREHYB, STR_OUT_MAXLEN);
  }
  /*if matrix memory is not inizialized, append it to the structure */
  if( knt_ptr->kntmatrix == NULL )
  {
    KNTadd_matrix ( knt_ptr );
  }
  /* determine knot_id of  whole chain (see KNT_defaults.h) */
  if ( st_p != DONT_CARE && end_p != DONT_CARE )
  {
    tmp = ( *close_subchain )( knt_ptr , knt_rect->index[st_p], knt_rect->index[end_p] );
    k_id = KNTLknot_determination ( &tmp, alx_wspace );
    KNTfree_arc ( &tmp );
  }
  else
  {
    k_id = KNTLknot_determination( knt_ptr , alx_wspace );
  }
  if ( k_id == K_Un )
  {
    fprintf(stderr,"WARNING!\n In KNTLloc_shrt_knot_rect1 \n No knot is present between beads %d and %d on knt_rect\n",
        knt_rect->index[st_p], knt_rect->index[end_p]);
    return;
  }
  /*-----END of initialization routine checks ----------------------*/

  /*
   * Initialize f_matrix f = 1, 0, -1.
   * Initialize diagonal elements of knot_matrix.
   *
   * NOTE, this should work also when both
   * st_p and end_p equal DONT_CARE (-2).
   */
  f_matrix = s2t(knt_rect->len, knt_rect->len);
	/*
  for ( i = 0 ; i < knt_rect->len; i++ )
  {
    j = knt_rect->len - 1;

    if((end_p > i) && (i> st_p))
    {
      f_matrix[i][j] = -1;
    }
    else if((st_p > end_p) && ((i<end_p) || (i>st_p)) )
    {
      f_matrix[i][j] = -1;
    }
    else
    {
      f_matrix[i][j] = 1;
    }
    if ( knt_rect->kntmatrix[i][i] == DONT_CARE )
    {
      knt_rect->kntmatrix[i][i] = k_id;
    }
    for ( j = knt_rect->len - 2 ; j > 0 ; j-- )
    {
      f_matrix[i][j] = -1;
    }
  }
	*/
  /* --- define an array of arcs ---- */
  arcs_array_len = knt_rect->len * knt_rect->len;
  arcs_array = ( KNTarc * ) calloc ( arcs_array_len, sizeof( KNTarc_DEFAULT ));
  /*
   * let's impose a standard length to the arcs of
   * the array, so that no surprises shall arise
   * from qsort
   */
  if (arcs_array == NULL )
  {
    sprintf(fail_str,"\nfailed to alloc arcs_array\n in KNTLloc_shrtcnt_knot_rect2.\n array size: %d\n",arcs_array_len);
    failed(fail_str);
  }
  for ( k = 0 ; k < arcs_array_len ; k++ )
  {
    arcs_array[k] = KNTarc_DEFAULT;
    arcs_array[k].len = knt_ptr->len;
  }

	int wlen;
	int lst_p;
	int lend_p;
	int I;
	if(st_p!=DONT_CARE && end_p !=DONT_CARE)
	{
		wlen = (end_p > st_p) ?  end_p - st_p : end_p + knt_rect->len - st_p;
		lst_p=st_p;
		lend_p=end_p;
		//PEZZOTTO
		for ( i = 0 ; i < knt_rect->len; i++ )
		{
			for(j=knt_rect->len-1;j>0;j--)
			{
				f_matrix[i][j]=-1;
			}
		}
		f_matrix[lst_p][wlen]=1;
		//FINE PEZZOTTO
	}
	else
	{
		lend_p=knt_rect->len-1;
		lst_p=0;
		wlen=knt_rect->len-1;
		//PEZZOTTO
		for ( i = 0 ; i < knt_rect->len; i++ )
		{
			f_matrix[i][wlen]=1;
			for(j=wlen-1;j>0;j--)
			{
				f_matrix[i][j]=-1;
			}
		}
		//FINE PEZZOTTO
	}
  k=0;
  //for ( j = knt_rect->len - 1 ; j > 3 ; j-- )
  for ( j = wlen ; j > 3 ; j-- )
  {
    _continue_ = FALSE;
		//for ( i = 0 ; i < knt_rect->len ; i++ )
		for ( i = 0 ; i <= wlen ; i++ )
    {
      /*
       * The following condition guarantees that there exists
       * a continuos succession of stable knotted arc connecting next point
       * to the diagonal of the knot matrix
       *
       * C arrays begin with 0. So the element of matrix [i][j] refers to the
       * arc starting in i of length j+1 ( including start and end ).
       */
			I=(lst_p+i)%knt_rect->len;//e questo e` il baco...
      //if( (f_matrix[i][ j ] == TRUE ) || (f_matrix[ ( i - 1 + knt_rect->len )%knt_rect->len][j] == TRUE ) )
      if( (f_matrix[I][ j ] == TRUE ) || (f_matrix[ ( I - 1 + knt_rect->len )%knt_rect->len][j] == TRUE ) )
      {
        /*
        start = knt_rect->index[i];
        end   = ( i + j ) % knt_rect->len;
        end   = knt_rect->index[end];
        start = i;
        end   = ( i + j ) % knt_rect->len;
				*/
        start = I;
        end   = ( I + j ) % knt_rect->len;
				r_start=knt_rect->index[start];
				r_end=knt_rect->index[end];
        /*
         * check the matrix to see if the topology of the two arcs
         * has already been determined. If not do it.
         */
        if(knt_ptr->kntmatrix[r_start][r_end] == DONT_CARE)
        {
          arc = ( *close_subchain )( knt_ptr , r_start, r_end );
          knt_ptr->kntmatrix[r_start][r_end] = KNTLknot_determination ( &arc, alx_wspace );
        }
        if(knt_ptr->kntmatrix[r_end][r_start] == DONT_CARE)
        {
          if ( (knt_rect->len - j)  < 4 ) // no knots possible
          {
            knt_ptr->kntmatrix[r_end][r_start] = K_Un;
          }
          else
          {
            cmp_arc = ( *close_subchain )( knt_ptr , r_end, r_start );
            knt_ptr->kntmatrix[r_end][r_start] = KNTLknot_determination ( &cmp_arc, alx_wspace );
          }
        }
        /*
         * If a stable arc set f_matrix to TRUE and flag _continue_ to true.
         */
        if(knt_ptr->kntmatrix[r_start][r_end] == k_id && knt_ptr->kntmatrix[r_end][r_start] == K_Un )
        {
					arcs_array[k].len 	= (r_end > r_start)? r_end - r_start : r_end + knt_ptr->len -r_start;
          arcs_array[k].start = start;
          arcs_array[k].end   = end;
          k++;
          f_matrix[i][j-1] = TRUE;
          _continue_     = TRUE;
          /*
           * If I find a knot which corresponds to st_p and end_p
           * I am done.
           *
           * --RIPENSANDOCI, MI CONVINCE POCO!
           */
          if ( start == st_p && end == end_p )
          {
            _continue_ = FALSE;
          }
        }
        else
        {
          f_matrix[i][j-1] = FALSE;
        }
      }
      /* free coordinates in arc and cmp_arc */
      KNTfree_arc ( &arc     );
      KNTfree_arc ( &cmp_arc );
      /* reset arc and cmp_arc */
      KNTreset    ( &arc     );
      KNTreset    ( &cmp_arc );
    }
    if ( !_continue_ )
    {
      break;
    }
  }

  /*
   * in theory qsort works also for lengths shorter
   * than the array length. anyway the default length is knt_ptr->length,
   * so I can sort the whole array
   */
  qsort ( arcs_array, arcs_array_len , sizeof(arcs_array[0]),compare_len);
	knot_len=knt_ptr->len;
	for(i=0;i<k;i++)
	{
		if ( st_p != DONT_CARE && end_p != DONT_CARE)
		{
			if( check_inclusion(arcs_array[i].start, arcs_array[i].end, st_p, end_p) == FALSE)
			{
				continue;
			}
		}

		klen = arcs_array[i].len;

#if DEBUG == 2
		fprintf(stderr,"array length: %d\n",k);
		fprintf (stderr,"start %d end %d len %d\n",arcs_array[i].start, arcs_array[i].end,arcs_array[i].len);
		fprintf (stderr,"start %d end %d len %d\n",arcs_array[k-1].start, arcs_array[k-1].end,arcs_array[k-1].len);
		for ( j = knt_rect->len - 1 ; j > 0 ; j-- )
		{
			for ( l = 0 ; l < knt_rect->len; l++ )
			{
				fprintf(stderr,"%4d %4d %2d\n ",l,j,f_matrix[l][j]);
			}
		}
#endif

		if(klen<=knot_len)
		{
			knot_len=klen;
			/*copy everything on knt_end */
			r_start=knt_rect->index[arcs_array[i].start];
			r_end=knt_rect->index[arcs_array[i].end];
			arcs_array[i].start=r_start;
			arcs_array[i].end=r_end;
			knt_end = KNTpush ( knt_ptr, &(arcs_array[i]));
			KNTarc_SET_value(knt_end, arc_type, ARC_ID_SCK, char);
			KNTarc_SET_value(knt_end, Adet_1,knt_ptr->Adet_1,int);
			KNTarc_SET_value(knt_end, Adet_2,knt_ptr->Adet_2,int);
			strncpy(knt_end->closure,str_cls,strlen(str_cls));
			strncpy(knt_end->simplification,SIMPTOPO,strlen(SIMPRECT));
		}
		else{
			break;
		}
	}

  free_s2t ( f_matrix );

  if(qhull_local_init)
  {
    qhull_terminate();
    qhull_is_init = 0;
  }
  else
  {
    qhull_free_mem();
  }

  free ( arcs_array );
}
/*
 * KNTLloc_shrtcnt_knot_rect2
 *
 * search a simplified chain with the top-down scheme.
 * if st_p and end_p are given, the search starts
 * OUTSIDE the region identified by these two points.
 */
void KNTLloc_shrtcnt_knot_rect2 ( KNTarc * knt_ptr, KNTarc * knt_rect, int st_p, int end_p, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace)
{
  int        i, j, k, k_id;
  int        start, end, klen_good, wlen;
  int        start_real, end_real;
  int         sel;
  int        _continue_       = FALSE;
  int        qhull_local_init = FALSE;
  char       str_cls[STR_OUT_MAXLEN] = "Undefined";
  char       fail_str[128];
  KNTarc     arc = KNTarc_DEFAULT;
  KNTarc     cmp_arc = KNTarc_DEFAULT;    /* arc and complementar arc */
  KNTarc       tmp     = KNTarc_DEFAULT;
  KNTarc     *  knt_end;
  short      ** f_matrix;
  int        arcs_array_len;
  KNTarc     * arcs_array;

  /*-----STARTING  routine checks ----------------------------------*/
  /*check if structure is correctly initialized */

  if( knt_ptr->is_init != 1 || knt_ptr->flag_len != 1 || knt_ptr->coord == NULL )
  {
    failed ( "KNTLloc_shrtcnt_knot_rect2. knt_ptr is not correctly initialized!\n");
  }
  if( knt_rect->is_init != 1 || knt_rect->flag_len != 1 || knt_rect->coord == NULL )
  {
    failed ( "KNTLloc_shrtcnt_knot_rect2. knt_rect is not correctly initialized!\n");
  }
  /* check closure and inizialize qhull if needed */
  if ( close_subchain == &KNTCqhull_hybrid_close_subchain  )
  {
    /*
     * if qhull is not initialized, initialize it.
     */
    if(!qhull_is_init)
    {
      qhull_init(knt_rect->len*3);
      qhull_local_init = TRUE;
    }
    strncpy ( str_cls,QHULLCLOSUREHYB, STR_OUT_MAXLEN);
  }
  /*if matrix memory is not inizialized, append it to the structure */
  if( knt_rect->kntmatrix == NULL )
  {
    KNTadd_matrix ( knt_rect );
  }
  /* determine knot_id of  whole chain (see KNT_defaults.h) */
  if ( st_p != DONT_CARE && end_p != DONT_CARE )
  {
    tmp = ( *close_subchain )( knt_rect , st_p, end_p );
    k_id = KNTLknot_determination ( &tmp, alx_wspace );
    KNTfree_arc ( &tmp );
  }
  else
  {
    k_id = KNTLknot_determination( knt_rect , alx_wspace );
  }
  if ( k_id == K_Un )
  {
    fprintf(stderr,"WARNING!\n In KNTLloc_shrtcnt_knot_rect2 \n No knot is present between beads %d and %d on knt_rect\n",
        st_p, end_p);
    return;
  }
  /*-----END of initialization routine checks ----------------------*/

  /*
   * Initialize f_matrix f = 1, 0, -1.
   * Initialize diagonal elements of knot_matrix.
   *
   * NOTE, this should work also when both
   * st_p and end_p equal DONT_CARE (-2).
   */
  f_matrix = s2t(knt_rect->len, knt_rect->len);
  for ( i = 0 ; i < knt_rect->len; i++ )
  {
    j = knt_rect->len - 1;

    if((end_p > i) && (i> st_p))
    {
      f_matrix[i][j] = -1;
    }
    else if((st_p > end_p) && ((i<end_p) || (i>st_p)) )
    {
      f_matrix[i][j] = -1;
    }
    else
    {
      f_matrix[i][j] = 1;
    }
    if ( knt_rect->kntmatrix[i][i] == DONT_CARE )
    {
      knt_rect->kntmatrix[i][i] = k_id;
    }
    for ( j = knt_rect->len - 2 ; j > 0 ; j-- )
    {
      f_matrix[i][j] = -1;
    }
  }
	//this shouldn't be necessary...anyway..
	f_matrix[st_p][end_p]=1;
  /* --- define an array of arcs ---- */
  arcs_array_len = knt_rect->len * knt_rect->len;
  arcs_array = ( KNTarc * ) calloc ( arcs_array_len, sizeof( KNTarc_DEFAULT ));
  /*
   * let's impose a standard length to the arcs of
   * the array, so that no surprises shall arise
   * from qsort
   */
  if (arcs_array == NULL )
  {
    sprintf(fail_str,"\nfailed to alloc arcs_array\n in KNTLloc_shrtcnt_knot_rect2.\n array size: %d\n",arcs_array_len);
    failed(fail_str);
  }
  for ( k = 0 ; k < arcs_array_len ; k++ )
  {
    arcs_array[k] = KNTarc_DEFAULT;
    arcs_array[k].len = knt_ptr->len;
  }

  wlen = (end_p > st_p) ?  end_p - st_p : end_p + knt_rect->len - st_p;
  k=0;
  //for ( j = knt_rect->len - 1 ; j > 3 ; j-- )
  for ( j = wlen  ; j > 3 ; j-- )
  {
    _continue_ = FALSE;
    //for ( i = 0 ; i < knt_rect->len ; i++ )
    for ( i = 0 ; i < wlen ; i++ )
    {
      /*
       * The following condition guarantees that there exists
       * a continuos succession of stable knotted arc connecting next point
       * to the diagonal of the knot matrix
       *
       * C arrays begin with 0. So the element of matrix [i][j] refers to the
       * arc starting in i of length j+1 ( including start and end ).
       */
      if( (f_matrix[(st_p+i)%knt_rect->len][ j ] == TRUE ) || (f_matrix[ ( st_p+i - 1 + knt_rect->len )%knt_rect->len][j] == TRUE ) )
      {
        /*
        start = knt_rect->index[i];
        end   = ( i + j ) % knt_rect->len;
        end   = knt_rect->index[end];
        */
        //start = i;
        //end   = ( i + j ) % knt_rect->len;
        start = (st_p+i) % knt_rect->len;
        end   = ( start + j ) % knt_rect->len;
        /*
         * check the matrix to see if the topology of the two arcs
         * has already been determined. If not do it.
         */
        if(knt_rect->kntmatrix[start][end] == DONT_CARE)
        {
          arc = ( *close_subchain )( knt_rect , start, end );
          knt_rect->kntmatrix[start][end] = KNTLknot_determination ( &arc, alx_wspace );
        }
        if(knt_rect->kntmatrix[end][start] == DONT_CARE)
        {
          if ( (knt_rect->len - j)  < 4 ) // no knots possible
          {
            knt_rect->kntmatrix[end][start] = K_Un;
          }
          else
          {
            cmp_arc = ( *close_subchain )( knt_rect , end, start );
            knt_rect->kntmatrix[end][start] = KNTLknot_determination ( &cmp_arc, alx_wspace );
          }
        }
        /*
         * If a stable arc set f_matrix to TRUE and flag _continue_ to true.
         */
        if(knt_rect->kntmatrix[start][end] == k_id && knt_rect->kntmatrix[end][start] == K_Un )
        {
          start_real = knt_rect->index[start];
          end_real   = knt_rect->index[end];
          arcs_array[k].len   = ( end_real > start_real ) ? end_real - start_real : end_real + knt_ptr->len - start_real;
          arcs_array[k].start = start;
          arcs_array[k].end   = end;
          k++;
          f_matrix[i][j-1] = TRUE;
          _continue_     = TRUE;
          /*
           * If I find a knot which corresponds to st_p and end_p
           * I am done.
           *
           * --RIPENSANDOCI, MI CONVINCE POCO!
           */
          if ( start == st_p && end == end_p )
          {
            _continue_ = FALSE;
          }
        }
        else
        {
          f_matrix[i][j-1] = FALSE;
        }
      }
      /* free coordinates in arc and cmp_arc */
      KNTfree_arc ( &arc     );
      KNTfree_arc ( &cmp_arc );
      /* reset arc and cmp_arc */
      KNTreset    ( &arc     );
      KNTreset    ( &cmp_arc );
    }
    if ( !_continue_ )
    {
      break;
    }
  }

  /*
   * in theory qsort works also for lengths shorter
   * than the array length. anyway the default length is knt_ptr->length,
   * so I can sort the whole array
   */
  qsort ( arcs_array, arcs_array_len , sizeof(arcs_array[0]),compare_len);
  sel = 0;
  if ( st_p != DONT_CARE && end_p != DONT_CARE)
  {
    while ( check_inclusion(arcs_array[sel].start, arcs_array[sel].end, st_p, end_p) == FALSE)
    {
      sel++;
    }
  }

  klen_good = arcs_array[sel].len;

#if DEBUG == 2
  fprintf(stderr,"array length: %d\n",k);
  fprintf (stderr,"start %d end %d len %d\n",arcs_array[sel].start, arcs_array[sel].end,arcs_array[sel].len);
  fprintf (stderr,"start %d end %d len %d\n",arcs_array[k-1].start, arcs_array[k-1].end,arcs_array[k-1].len);
#endif
#if DEBUG == 2
  for ( j = knt_rect->len - 1 ; j > 0 ; j-- )
  {
    for ( i = 0 ; i < knt_rect->len; i++ )
    {
      fprintf(stderr,"%4d %4d %2d\n ",i,j,f_matrix[i][j]);
    }
  }
#endif

  /*copy everything on knt_end */
  knt_end = KNTpush ( knt_rect, &(arcs_array[sel]));
  KNTarc_SET_value(knt_end, arc_type, ARC_ID_SCK, char);
  KNTarc_SET_value(knt_end, Adet_1,knt_rect->Adet_1,int);
  KNTarc_SET_value(knt_end, Adet_2,knt_rect->Adet_2,int);
  strncpy(knt_end->closure,str_cls,strlen(str_cls));
  strncpy(knt_end->simplification,SIMPTOPO,strlen(SIMPTOPO));

  free_s2t ( f_matrix );

  if(qhull_local_init)
  {
    qhull_terminate();
    qhull_is_init = 0;
  }
  else
  {
    qhull_free_mem();
  }

  free ( arcs_array );
}
void KNTLloc_shrt_knot_rect1 ( KNTarc * knt_ptr, KNTarc * knt_rect, int st_p, int end_p, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace )
{
  int          i, j, k,  start, end,arclen,len;
  int          k_id;
  int          qhull_local_init = FALSE;
  char         str_cls[STR_OUT_MAXLEN] = "Undefined";
  char         fail_str[128];
  //int          last_cycle = FALSE;
  KNTarc     * knt_end;
  KNTarc       arc     = KNTarc_DEFAULT;
  KNTarc       cmp_arc = KNTarc_DEFAULT;
  KNTarc       tmp     = KNTarc_DEFAULT;
  /* used in the FY shuffle */
  extern long int * ptr_idum;
  /*
   * I use this to perform a sorting of arcs with qsort
   */
  int          arcs_array_len;
  KNTarc     * arcs_array;
	int r_start, r_end;
	int one_knot_found;
	int knot_len;

  /*-----STARTING  routine checks ----------------------------------*/
  /*check if structure is correctly initialized */

  if( knt_ptr->is_init != 1 || knt_ptr->flag_len != 1 || knt_ptr->coord == NULL )
  {
    failed ( "KNTLloc_shrt_knot_rect. knt_ptr is not correctly initialized!\n");
  }
  if( knt_rect->is_init != 1 || knt_rect->flag_len != 1 || knt_rect->coord == NULL )
  {
    failed ( "KNTLloc_shrt_knot_rect. knt_rect is not correctly initialized!\n");
  }

  if ( close_subchain == &KNTCqhull_hybrid_close_subchain )
  {
    /*
     * if qhull is not initialized, initialize it.
     */
    if(!qhull_is_init)
    {
      qhull_init(knt_rect->len*3);
      qhull_local_init = TRUE;
    }
    strncpy ( str_cls,QHULLCLOSUREHYB, STR_OUT_MAXLEN);
  }
  /*if knt_ptr->kntmatrix memory is not inizialized, append it to the structure */
  if( knt_ptr->kntmatrix == NULL )
  {
    KNTadd_matrix ( knt_ptr );
  }
  /* determine knot_id of  whole chain (see KNT_defaults.h) */
	//This must be done on the unsimplified chain!!!
  if ( st_p != DONT_CARE && end_p != DONT_CARE )
  {
    tmp = ( *close_subchain )( knt_ptr , knt_rect->index[st_p], knt_rect->index[end_p] );
    k_id = KNTLknot_determination ( &tmp, alx_wspace );
    KNTfree_arc ( &tmp );
  }
  else
  {
    k_id = KNTLknot_determination( knt_ptr , alx_wspace );
  }
  if ( k_id == K_Un )
  {
    fprintf(stderr,"WARNING!\n In KNTLloc_shrt_knot_rect1 \n No knot is present between beads %d and %d on knt_rect\n",
        knt_rect->index[st_p], knt_rect->index[end_p]);
    return;
  }
  /*-----END of initialization routine checks ----------------------*/
  /*----generate sorted array of arcs ------------------------------*/
  arcs_array_len = knt_rect->len * knt_rect->len;
  arcs_array = ( KNTarc * ) calloc ( arcs_array_len, sizeof( KNTarc_DEFAULT ));
  if (arcs_array == NULL )
  {
    sprintf(fail_str,"\nfailed to alloc arcs_array\n in KNTLloc_shrt_knot_rect1.\n array size: %d\n",arcs_array_len);
    failed(fail_str);
  }
  k=0;
  for ( i = 0 ; i < knt_rect->len; i++ )
  {
    for ( j = 0 ; j < knt_rect->len ; j++ )
    {
      start = knt_rect->index[i];
      end   = knt_rect->index[j];
      arcs_array[k] = KNTarc_DEFAULT;
      arcs_array[k].start = i;
      arcs_array[k].end   = j;
      if ( st_p < 0 || end_p < 0 )
      {
         arcs_array[k].len   = ( end > start ) ? end - start : end + knt_ptr->len - start ;
      }
      else if ( st_p < end_p && i >= st_p && j<= end_p && i < j)
      {
        arcs_array[k].len   = ( end > start ) ? end - start : end + knt_ptr->len - start ;
      }
      else if ( st_p >= end_p && !( ( i > end_p && i < st_p) || (  j > end_p &&  j < st_p) || ( i == end_p &&  j == st_p )) )
      {
        arcs_array[k].len   = ( end > start ) ? end - start : end + knt_ptr->len - start ;
      }
      else
      {
        arcs_array[k].len =  knt_ptr->len;
      }
      k++;
    }
  }
  /*
   * Qsort reorders the array and gives as  a result an array of
   * simplified arcs ordered according to their corresponding length
   * on the original ring, from smaller to bigger length
   */
  //qsort ( arcs_array,arcs_array_len, sizeof(arcs_array[0]),compare_len);
  qsort ( arcs_array,k, sizeof(arcs_array[0]),compare_len);

	one_knot_found=0;
	knot_len=knt_ptr->len;
  for ( i = 0 ; i < k ; i++ )
  {
    start  = arcs_array[i].start;
    end    = arcs_array[i].end;
    len    = arcs_array[i].len;
    arclen = ( end > start) ? end - start : end + knt_rect->len - start;

		r_start= knt_rect->index[start];
		r_end	 = knt_rect->index[end];

		//questo dovrebbe salvare la degenerazione.
		if (one_knot_found && len > knot_len)
		{
			KNTfree_arc ( &arc );
			KNTfree_arc ( &cmp_arc );
			break;
		}
		//
    if(arclen < 4 )
    {
      continue;
    }
    /* set arc and cmp_arc to default knot as a safety measure */
    arc     = KNTarc_DEFAULT;
    cmp_arc = KNTarc_DEFAULT;
    /* primary arc */
    if ( knt_ptr->kntmatrix[r_start][r_end] == DONT_CARE )
    {
      if ( len < 4 || arclen < 4 )
      {
        knt_ptr->kntmatrix[r_start][r_end] = K_Un;
      }
      else if ( arclen == knt_rect->len )
      {
        knt_ptr->kntmatrix[r_start][r_end] = k_id;
      }
      else
      {
        arc = ( *close_subchain )( knt_ptr , r_start, r_end );
        knt_ptr->kntmatrix[r_start][r_end] = KNTLknot_determination ( &arc, alx_wspace );
      }
    }
    /* complementar arc */
    if( knt_ptr->kntmatrix[r_start][r_end] == k_id && knt_ptr->kntmatrix[r_end][r_start] == DONT_CARE)
    {
      if( knt_rect->len - arclen < 4  )
      {
        knt_ptr->kntmatrix[r_end][r_start] = K_Un;
      }
      else
      {
        cmp_arc = ( *close_subchain )( knt_ptr , r_end, r_start );
        knt_ptr->kntmatrix[r_end][r_start] = KNTLknot_determination ( &cmp_arc, alx_wspace );
      }
    }
    /* check if arc is knotted */
    if(knt_ptr->kntmatrix[r_start][r_end] == k_id && knt_ptr->kntmatrix[r_end][r_start] == K_Un )
		{
			tmp = ( *close_subchain )( knt_ptr, r_start, r_end );
			knt_end = KNTpush ( knt_ptr, &tmp);
			KNTfree_arc(&tmp);
			/* I do not want to pass coordinates */
			free_d2t(knt_end->coord);
			knt_end->coord = NULL;
			KNTarc_SET_value(knt_end, arc_type, ARC_ID_SK, char);
			KNTarc_SET_value(knt_end, Adet_1,knt_ptr->Adet_1,int);
			KNTarc_SET_value(knt_end, Adet_2,knt_ptr->Adet_2,int);
			/*--*/
			//KNTarc_SET_value(knt_end, len,len,int);
			strncpy(knt_end->closure,str_cls,strlen(str_cls));
			strncpy(knt_end->simplification,SIMPRECT,strlen(SIMPRECT));
			one_knot_found=1;
			knot_len=len;

		}
    /* free coordinates in arc and cmp_arc */
    KNTfree_arc ( &arc );
    KNTfree_arc ( &cmp_arc );
    /* reset arc and cmp_arc */
    KNTreset    ( &arc     );
    KNTreset    ( &cmp_arc );
  }

  if(qhull_local_init)
  {
    qhull_terminate();
    qhull_is_init = 0;
  }
  else
  {
    qhull_free_mem();
  }

  free ( arcs_array );

}

void KNTLloc_shrt_knot_unsafe_rect1 ( KNTarc * knt_ptr, KNTarc * knt_rect, int st_p, int end_p, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace )
{
  int          i, j, k,  start, end,arclen,len;
  int          k_id;
  int          qhull_local_init = FALSE;
  char         str_cls[STR_OUT_MAXLEN] = "Undefined";
  char         fail_str[128];
  //int          last_cycle = FALSE;
  KNTarc     * knt_end;
  KNTarc       arc     = KNTarc_DEFAULT;
  KNTarc       cmp_arc = KNTarc_DEFAULT;
  KNTarc       tmp     = KNTarc_DEFAULT;
  /* used in the FY shuffle */
  extern long int * ptr_idum;
  /*
   * I use this to perform a sorting of arcs with qsort
   */
  int          arcs_array_len;
  KNTarc     * arcs_array;
	int r_start, r_end;
	int one_knot_found;
	int knot_len;

  /*-----STARTING  routine checks ----------------------------------*/
  /*check if structure is correctly initialized */

  if( knt_ptr->is_init != 1 || knt_ptr->flag_len != 1 || knt_ptr->coord == NULL )
  {
    failed ( "KNTLloc_shrt_knot_rect. knt_ptr is not correctly initialized!\n");
  }
  if( knt_rect->is_init != 1 || knt_rect->flag_len != 1 || knt_rect->coord == NULL )
  {
    failed ( "KNTLloc_shrt_knot_rect. knt_rect is not correctly initialized!\n");
  }

  if ( close_subchain == &KNTCqhull_hybrid_close_subchain )
  {
    /*
     * if qhull is not initialized, initialize it.
     */
    if(!qhull_is_init)
    {
      qhull_init(knt_rect->len*3);
      qhull_local_init = TRUE;
    }
    strncpy ( str_cls,QHULLCLOSUREHYB, STR_OUT_MAXLEN);
  }
  /*if knt_ptr->kntmatrix memory is not inizialized, append it to the structure */
  if( knt_ptr->kntmatrix == NULL )
  {
    KNTadd_matrix ( knt_ptr );
  }
  /* determine knot_id of  whole chain (see KNT_defaults.h) */
	//This must be done on the unsimplified chain!!!
  if ( st_p != DONT_CARE && end_p != DONT_CARE )
  {
    tmp = ( *close_subchain )( knt_ptr , knt_rect->index[st_p], knt_rect->index[end_p] );
    k_id = KNTLknot_determination ( &tmp, alx_wspace );
    KNTfree_arc ( &tmp );
  }
  else
  {
    k_id = KNTLknot_determination( knt_ptr , alx_wspace );
  }
  if ( k_id == K_Un )
  {
    fprintf(stderr,"WARNING!\n In KNTLloc_shrt_knot_rect1 \n No knot is present between beads %d and %d on knt_rect\n",
        knt_rect->index[st_p], knt_rect->index[end_p]);
    return;
  }
  /*-----END of initialization routine checks ----------------------*/
  /*----generate sorted array of arcs ------------------------------*/
  arcs_array_len = knt_rect->len * knt_rect->len;
  arcs_array = ( KNTarc * ) calloc ( arcs_array_len, sizeof( KNTarc_DEFAULT ));
  if (arcs_array == NULL )
  {
    sprintf(fail_str,"\nfailed to alloc arcs_array\n in KNTLloc_shrt_knot_rect1.\n array size: %d\n",arcs_array_len);
    failed(fail_str);
  }
  k=0;
  for ( i = 0 ; i < knt_rect->len; i++ )
  {
    for ( j = 0 ; j < knt_rect->len ; j++ )
    {
      start = knt_rect->index[i];
      end   = knt_rect->index[j];
      arcs_array[k] = KNTarc_DEFAULT;
      arcs_array[k].start = i;
      arcs_array[k].end   = j;
      if ( st_p < 0 || end_p < 0 )
      {
         arcs_array[k].len   = ( end > start ) ? end - start : end + knt_ptr->len - start ;
      }
      else if ( st_p < end_p && i >= st_p && j<= end_p && i < j)
      {
        arcs_array[k].len   = ( end > start ) ? end - start : end + knt_ptr->len - start ;
      }
      else if ( st_p >= end_p && !( ( i > end_p && i < st_p) || (  j > end_p &&  j < st_p) || ( i == end_p &&  j == st_p )) )
      {
        arcs_array[k].len   = ( end > start ) ? end - start : end + knt_ptr->len - start ;
      }
      else
      {
        arcs_array[k].len =  knt_ptr->len;
      }
      k++;
    }
  }
  /*
   * Qsort reorders the array and gives as  a result an array of
   * simplified arcs ordered according to their corresponding length
   * on the original ring, from smaller to bigger length
   */
  //qsort ( arcs_array,arcs_array_len, sizeof(arcs_array[0]),compare_len);
  qsort ( arcs_array,k, sizeof(arcs_array[0]),compare_len);

	one_knot_found=0;
	knot_len=knt_ptr->len;
  for ( i = 0 ; i < k ; i++ )
  {
    start  = arcs_array[i].start;
    end    = arcs_array[i].end;
    len    = arcs_array[i].len;
    arclen = ( end > start) ? end - start : end + knt_rect->len - start;

		r_start= knt_rect->index[start];
		r_end	 = knt_rect->index[end];

		//questo dovrebbe salvare la degenerazione.
		if (one_knot_found && len > knot_len)
		{
			KNTfree_arc ( &arc );
			KNTfree_arc ( &cmp_arc );
			break;
		}
		//
    if(arclen < 4 )
    {
      continue;
    }
    /* set arc and cmp_arc to default knot as a safety measure */
    arc     = KNTarc_DEFAULT;
    cmp_arc = KNTarc_DEFAULT;
    /* primary arc */
    if ( knt_ptr->kntmatrix[r_start][r_end] == DONT_CARE )
    {
      if ( len < 4 || arclen < 4 )
      {
        knt_ptr->kntmatrix[r_start][r_end] = K_Un;
      }
      else if ( arclen == knt_rect->len )
      {
        knt_ptr->kntmatrix[r_start][r_end] = k_id;
      }
      else
      {
        arc = ( *close_subchain )( knt_ptr , r_start, r_end );
        knt_ptr->kntmatrix[r_start][r_end] = KNTLknot_determination ( &arc, alx_wspace );
      }
    }
    /* check if arc is knotted */
    if(knt_ptr->kntmatrix[r_start][r_end] == k_id)
		{
			tmp = ( *close_subchain )( knt_ptr, r_start, r_end );
			knt_end = KNTpush ( knt_ptr, &tmp);
			KNTfree_arc(&tmp);
			/* I do not want to pass coordinates */
			free_d2t(knt_end->coord);
			knt_end->coord = NULL;
			KNTarc_SET_value(knt_end, arc_type, ARC_ID_SKU, char);
			KNTarc_SET_value(knt_end, Adet_1,knt_ptr->Adet_1,int);
			KNTarc_SET_value(knt_end, Adet_2,knt_ptr->Adet_2,int);
			/*--*/
			//KNTarc_SET_value(knt_end, len,len,int);
			strncpy(knt_end->closure,str_cls,strlen(str_cls));
			strncpy(knt_end->simplification,SIMPRECT,strlen(SIMPRECT));
			one_knot_found=1;
			knot_len=len;

		}
    /* free coordinates in arc and cmp_arc */
    KNTfree_arc ( &arc );
    KNTfree_arc ( &cmp_arc );
    /* reset arc and cmp_arc */
    KNTreset    ( &arc     );
    KNTreset    ( &cmp_arc );
  }

  if(qhull_local_init)
  {
    qhull_terminate();
    qhull_is_init = 0;
  }
  else
  {
    qhull_free_mem();
  }

  free ( arcs_array );

}
/*
 * KNTLloc_shrt_knot_rect
 *
 * Performs the same operations done by
 * KNTLloc_shrt_knot, but uses chain rectification
 * to cancel the unimportant points.
 *
 * Topologies are computed on the simplified chain. When a shortest arc is found,
 * its topology on the original unsimplified chain is computed.
 *
 */

void KNTLloc_shrt_knot_rect2 ( KNTarc * knt_ptr, KNTarc * knt_rect, int st_p, int end_p, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace )
{
  int          i, j, k,  start, end,arclen,len;
  int          k_id;
  int          qhull_local_init = FALSE;
  char         str_cls[STR_OUT_MAXLEN] = "Undefined";
  char         fail_str[128];
  //int          last_cycle = FALSE;
  KNTarc     * knt_end;
  KNTarc       arc     = KNTarc_DEFAULT;
  KNTarc       cmp_arc = KNTarc_DEFAULT;
  KNTarc       tmp     = KNTarc_DEFAULT;
  /* used in the FY shuffle */
  extern long int * ptr_idum;
  /*
   * I use this to perform a sorting of arcs with qsort
   */
  int          arcs_array_len;
  KNTarc     * arcs_array;

  /*-----STARTING  routine checks ----------------------------------*/
  /*check if structure is correctly initialized */

  if( knt_ptr->is_init != 1 || knt_ptr->flag_len != 1 || knt_ptr->coord == NULL )
  {
    failed ( "KNTLloc_shrt_knot_rect. knt_ptr is not correctly initialized!\n");
  }
  if( knt_rect->is_init != 1 || knt_rect->flag_len != 1 || knt_rect->coord == NULL )
  {
    failed ( "KNTLloc_shrt_knot_rect. knt_rect is not correctly initialized!\n");
  }

  if ( close_subchain == &KNTCqhull_hybrid_close_subchain )
  {
    /*
     * if qhull is not initialized, initialize it.
     */
    if(!qhull_is_init)
    {
      qhull_init(knt_rect->len*3);
      qhull_local_init = TRUE;
    }
    strncpy ( str_cls,QHULLCLOSUREHYB, STR_OUT_MAXLEN);
  }
  /*if knt_rect->kntmatrix memory is not inizialized, append it to the structure */
  if( knt_rect->kntmatrix == NULL )
  {
    KNTadd_matrix ( knt_rect );
  }
  /* determine knot_id of  whole chain (see KNT_defaults.h) */
  if ( st_p != DONT_CARE && end_p != DONT_CARE )
  {
    tmp = ( *close_subchain )( knt_rect , st_p, end_p );
    k_id = KNTLknot_determination ( &tmp, alx_wspace );
    KNTfree_arc ( &tmp );
  }
  else
  {
    k_id = KNTLknot_determination( knt_rect , alx_wspace );
  }
  if ( k_id == K_Un )
  {
    fprintf(stderr,"WARNING!\n In KNTLloc_shrt_knot_rect2 \n No knot is present between beads %d and %d on knt_rect\n",
        st_p, end_p);
    return;
  }
  /*-----END of initialization routine checks ----------------------*/
  /*----generate sorted array of arcs ------------------------------*/
  arcs_array_len = knt_rect->len * knt_rect->len;
  arcs_array = ( KNTarc * ) calloc ( arcs_array_len, sizeof( KNTarc_DEFAULT ));
  if (arcs_array == NULL )
  {
    sprintf(fail_str,"\nfailed to alloc arcs_array\n in KNTLloc_shrt_knot_rect2.\n array size: %d\n",arcs_array_len);
    failed(fail_str);
  }
  k=0;
  for ( i = 0 ; i < knt_rect->len; i++ )
  {
    for ( j = 0 ; j < knt_rect->len ; j++ )
    {
      start = knt_rect->index[i];
      end   = knt_rect->index[j];
      arcs_array[k] = KNTarc_DEFAULT;
      arcs_array[k].start = i;
      arcs_array[k].end   = j;
      if ( st_p < 0 || end_p < 0 )
      {
         arcs_array[k].len   = ( end > start ) ? end - start : end + knt_ptr->len - start ;
      }
      else if ( st_p < end_p && i >= st_p && j<= end_p && i < j)
      {
        arcs_array[k].len   = ( end > start ) ? end - start : end + knt_ptr->len - start ;
      }
      else if ( st_p >= end_p && !( ( i > end_p && i < st_p) || (  j > end_p &&  j < st_p) || ( i == end_p &&  j == st_p )) )
      {
        arcs_array[k].len   = ( end > start ) ? end - start : end + knt_ptr->len - start ;
      }
      else
      {
        arcs_array[k].len =  knt_ptr->len;
      }
      k++;
    }
  }
  /*
   * Let's shuffle the array, just to be sure we don't introduce any
   * bias in the way the shortest element is found
   * note that k < arcs_array_len by construction.
   */
  /*
  for(i=k-1;i>=0;i--)
  {
     j = (int) floor ( ran2( ptr_idum ) * i );
     // k <- j
     arcs_array[k].start = arcs_array[j].start;
     arcs_array[k].end   = arcs_array[j].end;
     arcs_array[k].len   = arcs_array[j].len;
     // j <- i
     arcs_array[j].start = arcs_array[i].start;
     arcs_array[j].end   = arcs_array[i].end;
     arcs_array[j].len   = arcs_array[i].len;
     // i <- k
     arcs_array[i].start = arcs_array[k].start;
     arcs_array[i].end   = arcs_array[k].end;
     arcs_array[i].len   = arcs_array[k].len;
  }*/
  /*
   * Qsort reorders the array and gives as  a result an array of
   * simplified arcs ordered according to their corresponding length
   * on the original ring, from smaller to bigger length
   */
  //qsort ( arcs_array,arcs_array_len, sizeof(arcs_array[0]),compare_len);
  qsort ( arcs_array,k, sizeof(arcs_array[0]),compare_len);

  for ( k = 0 ; k < arcs_array_len ; k++ )
  {
    start  = arcs_array[k].start;
    end    = arcs_array[k].end;
    len    = arcs_array[k].len;
    arclen = ( end > start) ? end - start : end + knt_rect->len - start;
    if(arclen < 4 )
    {
      continue;
    }
    /* set arc and cmp_arc to default knot as a safety measure */
    arc     = KNTarc_DEFAULT;
    cmp_arc = KNTarc_DEFAULT;
    /* primary arc */
    if ( knt_rect->kntmatrix[start][end] == DONT_CARE )
    {
      if ( len < 4 || arclen < 4 )
      {
        knt_rect->kntmatrix[start][end] = K_Un;
      }
      else if ( arclen == knt_rect->len )
      {
        knt_rect->kntmatrix[start][end] = k_id;
      }
      else
      {
        arc = ( *close_subchain )( knt_rect , start, end );
        knt_rect->kntmatrix[start][end] = KNTLknot_determination ( &arc, alx_wspace );
      }
    }
    /* complementar arc */
    if( knt_rect->kntmatrix[start][end] == k_id && knt_rect->kntmatrix[end][start] == DONT_CARE)
    {
      if( knt_rect->len - arclen < 4  )
      {
        knt_rect->kntmatrix[end][start] = K_Un;
      }
      else
      {
        cmp_arc = ( *close_subchain )( knt_rect , end, start );
        knt_rect->kntmatrix[end][start] = KNTLknot_determination ( &cmp_arc, alx_wspace );
      }
    }
    /* check if arc is knotted */
    if(knt_rect->kntmatrix[start][end] == k_id && knt_rect->kntmatrix[end][start] == K_Un )
    {
      /*
      fprintf(stderr,
          "%d # %d %d\n",
          knt_rect->index[end]-knt_rect->index[start],
          knt_rect->index[start],knt_rect->index[end]);
      for(i=knt_rect->index[start]; i<=knt_rect->index[end]; i++)
      {
        fprintf(stderr,
            "%lf %lf %lf\n",
            knt_ptr->coord[i][0],knt_ptr->coord[i][1],knt_ptr->coord[i][2]);
      }
      */
			//PEZZOTTO!!!
			int start_o  = knt_rect->index[start];
    	int end_o    = knt_rect->index[end];
    	int arclen_o;
			int TMP_id, TMP_cmp_id;
			arclen_o = ( end_o > start_o) ? end_o - start_o : end_o + knt_ptr->len - start_o;
			if (knt_ptr->len-arclen_o <5)
			{
				TMP_cmp_id=K_Un;
			}
			else
			{
      	tmp = ( *close_subchain )( knt_ptr, end_o, start_o );
				TMP_cmp_id=KNTLknot_determination ( &tmp, alx_wspace );
				KNTfree_arc(&tmp);
			}
      tmp = ( *close_subchain )( knt_ptr, start_o, end_o );
			TMP_id=KNTLknot_determination ( &tmp, alx_wspace );
			KNTfree_arc(&tmp);
			if ( TMP_id == k_id && TMP_cmp_id == K_Un)
			{
				tmp = ( *close_subchain )( knt_rect, start, end );
				knt_end = KNTpush ( knt_rect, &tmp);
				KNTfree_arc(&tmp);
				/* I do not want to pass coordinates */
				/* On a second thought, they are pretty useful for debugging reasons */
				//free_d2t(knt_end->coord);
				//knt_end->coord = NULL;
				KNTarc_SET_value(knt_end, arc_type, ARC_ID_SK, char);
				KNTarc_SET_value(knt_end, Adet_1,knt_rect->Adet_1,int);
				KNTarc_SET_value(knt_end, Adet_2,knt_rect->Adet_2,int);
				/* substitute start and end with their equivalent on the real chain? */
				//KNTarc_SET_value(knt_end, start,knt_rect->index[start],int);
				//KNTarc_SET_value(knt_end, end,knt_rect->index[end],int);
				/*--*/
				//KNTarc_SET_value(knt_end, len,len,int);
				strncpy(knt_end->closure,str_cls,strlen(str_cls));
				strncpy(knt_end->simplification,SIMPTOPO,strlen(SIMPTOPO));

				/*
				 * insomma, me ne sbatto della degenerazione.
				 * QUESTO ANDREBBE RISOLTO, TENENDO LA DEGENERAZIONE!
				 * TROVARE UN SISTEMA
				 */
				KNTfree_arc ( &arc );
				KNTfree_arc ( &cmp_arc );
				break;
			}
    }
    /* free coordinates in arc and cmp_arc */
    KNTfree_arc ( &arc );
    KNTfree_arc ( &cmp_arc );
    /* reset arc and cmp_arc */
    KNTreset    ( &arc     );
    KNTreset    ( &cmp_arc );
  }

  if(qhull_local_init)
  {
    qhull_terminate();
    qhull_is_init = 0;
  }
  else
  {
    qhull_free_mem();
  }

  free ( arcs_array );

}

void KNTLloc_shrt_knot_unsafe_rect2 ( KNTarc * knt_ptr, KNTarc * knt_rect, int st_p, int end_p, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace )
{
	int          i, j, k,  start, end,arclen,len;
	int          k_id;
	int          qhull_local_init = FALSE;
	char         str_cls[STR_OUT_MAXLEN] = "Undefined";
	char         fail_str[128];
	//int          last_cycle = FALSE;
	KNTarc     * knt_end;
	KNTarc       arc     = KNTarc_DEFAULT;
	KNTarc       cmp_arc = KNTarc_DEFAULT;
	KNTarc       tmp     = KNTarc_DEFAULT;
	/* used in the FY shuffle */
	extern long int * ptr_idum;
	/*
	 * I use this to perform a sorting of arcs with qsort
	 */
	int          arcs_array_len;
	KNTarc     * arcs_array;

	/*-----STARTING  routine checks ----------------------------------*/
	/*check if structure is correctly initialized */

	if( knt_ptr->is_init != 1 || knt_ptr->flag_len != 1 || knt_ptr->coord == NULL )
	{
		failed ( "KNTLloc_shrt_knot_rect. knt_ptr is not correctly initialized!\n");
	}
	if( knt_rect->is_init != 1 || knt_rect->flag_len != 1 || knt_rect->coord == NULL )
	{
		failed ( "KNTLloc_shrt_knot_rect. knt_rect is not correctly initialized!\n");
	}

	if ( close_subchain == &KNTCqhull_hybrid_close_subchain )
	{
		/*
		 * if qhull is not initialized, initialize it.
		 */
		if(!qhull_is_init)
		{
			qhull_init(knt_rect->len*3);
			qhull_local_init = TRUE;
		}
		strncpy ( str_cls,QHULLCLOSUREHYB, STR_OUT_MAXLEN);
	}
	/*if knt_rect->kntmatrix memory is not inizialized, append it to the structure */
	if( knt_rect->kntmatrix == NULL )
	{
		KNTadd_matrix ( knt_rect );
	}
	/* determine knot_id of  whole chain (see KNT_defaults.h) */
	if ( st_p != DONT_CARE && end_p != DONT_CARE )
	{
		tmp = ( *close_subchain )( knt_rect , st_p, end_p );
		k_id = KNTLknot_determination ( &tmp, alx_wspace );
		KNTfree_arc ( &tmp );
	}
	else
	{
		k_id = KNTLknot_determination( knt_rect , alx_wspace );
	}
	if ( k_id == K_Un )
	{
		fprintf(stderr,"WARNING!\n In KNTLloc_shrt_knot_rect2 \n No knot is present between beads %d and %d on knt_rect\n",
				st_p, end_p);
		return;
	}
	/*-----END of initialization routine checks ----------------------*/
	/*----generate sorted array of arcs ------------------------------*/
	arcs_array_len = knt_rect->len * knt_rect->len;
	arcs_array = ( KNTarc * ) calloc ( arcs_array_len, sizeof( KNTarc_DEFAULT ));
	if (arcs_array == NULL )
	{
		sprintf(fail_str,"\nfailed to alloc arcs_array\n in KNTLloc_shrt_knot_rect2.\n array size: %d\n",arcs_array_len);
		failed(fail_str);
	}
	k=0;
	for ( i = 0 ; i < knt_rect->len; i++ )
	{
		for ( j = 0 ; j < knt_rect->len ; j++ )
		{
			start = knt_rect->index[i];
			end   = knt_rect->index[j];
			arcs_array[k] = KNTarc_DEFAULT;
			arcs_array[k].start = i;
			arcs_array[k].end   = j;
			if ( st_p < 0 || end_p < 0 )
			{
				 arcs_array[k].len   = ( end > start ) ? end - start : end + knt_ptr->len - start ;
			}
			else if ( st_p < end_p && i >= st_p && j<= end_p && i < j)
			{
				arcs_array[k].len   = ( end > start ) ? end - start : end + knt_ptr->len - start ;
			}
			else if ( st_p >= end_p && !( ( i > end_p && i < st_p) || (  j > end_p &&  j < st_p) || ( i == end_p &&  j == st_p )) )
			{
				arcs_array[k].len   = ( end > start ) ? end - start : end + knt_ptr->len - start ;
			}
			else
			{
				arcs_array[k].len =  knt_ptr->len;
			}
			k++;
		}
	}
	/*
	 * Qsort reorders the array and gives as  a result an array of
	 * simplified arcs ordered according to their corresponding length
	 * on the original ring, from smaller to bigger length
	 */
	//qsort ( arcs_array,arcs_array_len, sizeof(arcs_array[0]),compare_len);
	qsort ( arcs_array,k, sizeof(arcs_array[0]),compare_len);

	for ( k = 0 ; k < arcs_array_len ; k++ )
	{
		start  = arcs_array[k].start;
		end    = arcs_array[k].end;
		len    = arcs_array[k].len;
		arclen = ( end > start) ? end - start : end + knt_rect->len - start;
		if(arclen < 4 )
		{
			continue;
		}
		/* set arc and cmp_arc to default knot as a safety measure */
		arc     = KNTarc_DEFAULT;
		cmp_arc = KNTarc_DEFAULT;
		/* primary arc */
		if ( knt_rect->kntmatrix[start][end] == DONT_CARE )
		{
			if ( len < 4 || arclen < 4 )
			{
				knt_rect->kntmatrix[start][end] = K_Un;
			}
			else if ( arclen == knt_rect->len )
			{
				knt_rect->kntmatrix[start][end] = k_id;
			}
			else
			{
				arc = ( *close_subchain )( knt_rect , start, end );
				knt_rect->kntmatrix[start][end] = KNTLknot_determination ( &arc, alx_wspace );
			}
		}
		/* check if arc is knotted */
		if(knt_rect->kntmatrix[start][end] == k_id  )
		{
			/*
			fprintf(stderr,
					"%d # %d %d\n",
					knt_rect->index[end]-knt_rect->index[start],
					knt_rect->index[start],knt_rect->index[end]);
			for(i=knt_rect->index[start]; i<=knt_rect->index[end]; i++)
			{
				fprintf(stderr,
						"%lf %lf %lf\n",
						knt_ptr->coord[i][0],knt_ptr->coord[i][1],knt_ptr->coord[i][2]);
			}
			*/
			//PEZZOTTO!!!
			int start_o  = knt_rect->index[start];
			int end_o    = knt_rect->index[end];
			int arclen_o;
			int TMP_id, TMP_cmp_id;
			arclen_o = ( end_o > start_o) ? end_o - start_o : end_o + knt_ptr->len - start_o;
			if (knt_ptr->len-arclen_o <5)
			{
				TMP_cmp_id=K_Un;
			}
			else
			{
				tmp = ( *close_subchain )( knt_ptr, end_o, start_o );
				TMP_cmp_id=KNTLknot_determination ( &tmp, alx_wspace );
				KNTfree_arc(&tmp);
			}
			tmp = ( *close_subchain )( knt_ptr, start_o, end_o );
			TMP_id=KNTLknot_determination ( &tmp, alx_wspace );
			KNTfree_arc(&tmp);
			if ( TMP_id == k_id && TMP_cmp_id == K_Un)
			{
				tmp = ( *close_subchain )( knt_rect, start, end );
				knt_end = KNTpush ( knt_rect, &tmp);
				KNTfree_arc(&tmp);
				/* I do not want to pass coordinates */
				/* On a second thought, they are pretty useful for debugging reasons */
				//free_d2t(knt_end->coord);
				//knt_end->coord = NULL;
				KNTarc_SET_value(knt_end, arc_type, ARC_ID_SK, char);
				KNTarc_SET_value(knt_end, Adet_1,knt_rect->Adet_1,int);
				KNTarc_SET_value(knt_end, Adet_2,knt_rect->Adet_2,int);
				/* substitute start and end with their equivalent on the real chain? */
				//KNTarc_SET_value(knt_end, start,knt_rect->index[start],int);
				//KNTarc_SET_value(knt_end, end,knt_rect->index[end],int);
				/*--*/
				//KNTarc_SET_value(knt_end, len,len,int);
				strncpy(knt_end->closure,str_cls,strlen(str_cls));
				strncpy(knt_end->simplification,SIMPTOPO,strlen(SIMPTOPO));
				/*
				 * QUESTO ANDREBBE RISOLTO, TENENDO LA DEGENERAZIONE!
				 * TROVARE UN SISTEMA
				 */
				KNTfree_arc ( &arc );
				KNTfree_arc ( &cmp_arc );
				break;
			}
		}
		/* free coordinates in arc and cmp_arc */
		KNTfree_arc ( &arc );
		KNTfree_arc ( &cmp_arc );
		/* reset arc and cmp_arc */
		KNTreset    ( &arc     );
		KNTreset    ( &cmp_arc );
	}

	if(qhull_local_init)
	{
		qhull_terminate();
		qhull_is_init = 0;
	}
	else
	{
		qhull_free_mem();
	}

	free ( arcs_array );

}


/*
void KNTLloc_shrt_knot_unsafe_rect2 ( KNTarc * knt_ptr, KNTarc * knt_rect, int st_p, int end_p, KNTarc (*close_subchain) ( KNTarc *, int, int ), ALX_wspace * alx_wspace )
{
  int          i, j, k,  start, end,arclen,len;
  int          k_id;
  int          qhull_local_init = FALSE;
  char         str_cls[STR_OUT_MAXLEN] = "Undefined";
  char         fail_str[128];
  //int          last_cycle = FALSE;
  KNTarc     * knt_end;
  KNTarc       arc     = KNTarc_DEFAULT;
  KNTarc       tmp     = KNTarc_DEFAULT;
   // I use this to perform a sorting of arcs with qsort
  int          arcs_array_len;
  KNTarc     * arcs_array;

  //-----STARTING  routine checks ------------------
  //check if structure is correctly initialized 

  if( knt_ptr->is_init != 1 || knt_ptr->flag_len != 1 || knt_ptr->coord == NULL )
  {
    failed ( "KNTLloc_shrt_knot_rect. knt_ptr is not correctly initialized!\n");
  }
  if( knt_rect->is_init != 1 || knt_rect->flag_len != 1 || knt_rect->coord == NULL )
  {
    failed ( "KNTLloc_shrt_knot_rect. knt_rect is not correctly initialized!\n");
  }

  if ( close_subchain == &KNTCqhull_hybrid_close_subchain )
  {
     // if qhull is not initialized, initialize it.
    if(!qhull_is_init)
    {
      qhull_init(knt_rect->len*3);
      qhull_local_init = TRUE;
    }
    strncpy ( str_cls,QHULLCLOSUREHYB, STR_OUT_MAXLEN);
  }
  //if knt_rect->kntmatrix memory is not inizialized, append it to the structure 
  if( knt_rect->kntmatrix == NULL )
  {
    KNTadd_matrix ( knt_rect );
  }
  // determine knot_id of  whole chain (see KNT_defaults.h) 
  if ( st_p != DONT_CARE && end_p != DONT_CARE )
  {
    tmp = ( *close_subchain )( knt_rect , st_p, end_p );
    k_id = KNTLknot_determination ( &tmp, alx_wspace );
    KNTfree_arc ( &tmp );
  }
  else
  {
    k_id = KNTLknot_determination( knt_rect , alx_wspace );
  }
  if ( k_id == K_Un )
  {
    fprintf(stderr,"WARNING!\n In KNTLloc_shrt_knot_rect2 \n No knot is present between beads %d and %d on knt_rect\n",
        st_p, end_p);
    return;
  }
  //-----END of initialization routine checks ----------------------
  //----generate sorted array of arcs ------------------------------
  arcs_array_len = knt_rect->len * knt_rect->len;
  arcs_array = ( KNTarc * ) calloc ( arcs_array_len, sizeof( KNTarc_DEFAULT ));
  if (arcs_array == NULL )
  {
    sprintf(fail_str,"\nfailed to alloc arcs_array\n in KNTLloc_shrt_knot_rect2.\n array size: %d\n",arcs_array_len);
    failed(fail_str);
  }
  k=0;
  for ( i = 0 ; i < knt_rect->len ; i++ )
  {
    for ( j = 0 ; j < knt_rect->len ; j++ )
    {
      start = knt_rect->index[i];
      end   = knt_rect->index[j];
      arcs_array[k] = KNTarc_DEFAULT;
      arcs_array[k].start = i;
      arcs_array[k].end   = j;
      if ( st_p < 0 || end_p < 0 )
      {
         arcs_array[k].len   = ( end > start ) ? end - start : end + knt_ptr->len - start ;
      }
      else if ( st_p < end_p && i >= st_p && j<= end_p && i < j)
      {
        arcs_array[k].len   = ( end > start ) ? end - start : end + knt_ptr->len - start ;
      }
      else if ( st_p >= end_p && !( ( i > end_p && i < st_p) || (  j > end_p &&  j < st_p) || ( i == end_p &&  j == st_p )) )
      {
        arcs_array[k].len   = ( end > start ) ? end - start : end + knt_ptr->len - start ;
      }
      else
      {
        arcs_array[k].len =  knt_ptr->len;
      }
      k++;
    }
  }
   // Qsort reorders the array and gives as  a result an array of
   // simplified arcs ordered according to their corresponding length
   // on the original ring, from smaller to bigger length
  qsort ( arcs_array,arcs_array_len, sizeof(arcs_array[0]),compare_len);

  for ( k = 0 ; k < arcs_array_len ; k++ )
  {
    start  = arcs_array[k].start;
    end    = arcs_array[k].end;
    len    = arcs_array[k].len;
    arclen = ( end > start) ? end - start : end + knt_rect->len - start;
    if(arclen < 4 )
    {
      continue;
    }
    // set arc and cmp_arc to default knot as a safety measure 
    arc     = KNTarc_DEFAULT;
    // primary arc 
    if ( knt_rect->kntmatrix[start][end] == DONT_CARE )
    {
      if ( len < 4 || arclen < 4 )
      {
        knt_rect->kntmatrix[start][end] = K_Un;
      }
      else if ( arclen == knt_rect->len )
      {
        knt_rect->kntmatrix[start][end] = k_id;
      }
      else
      {
        arc = ( *close_subchain )( knt_rect , start, end );
        knt_rect->kntmatrix[start][end] = KNTLknot_determination ( &arc, alx_wspace );
      }
    }
    // check if arc is knotted 
    if(knt_rect->kntmatrix[start][end] == k_id )
    {
      //tmp = ( *close_subchain )( knt_rect, start, end );
      //knt_end = KNTpush ( knt_rect, &tmp);
      //KNTfree_arc(&tmp);
      //free_d2t(knt_end->coord);
      //knt_end->coord = NULL;
      //KNTarc_SET_value(knt_end, arc_type, ARC_ID_SKU, char);
      //KNTarc_SET_value(knt_end, Adet_1,knt_rect->Adet_1,int);
      //KNTarc_SET_value(knt_end, Adet_2,knt_rect->Adet_2,int);
      //KNTarc_SET_value(knt_end, start,knt_rect->index[start],int);
      //KNTarc_SET_value(knt_end, end,knt_rect->index[end],int);
      //KNTarc_SET_value(knt_end, len,len,int);
      //strncpy(knt_end->closure,str_cls,strlen(str_cls));
      //strncpy(knt_end->simplification,SIMPTOPO,strlen(SIMPTOPO));

      //break;
			//PEZZOTTO!!!
			int start_o  = knt_rect->index[start];
    	int end_o    = knt_rect->index[end];
    	int arclen_o;
			int TMP_id;
			arclen_o = ( end_o > start_o) ? end_o - start_o : end_o + knt_ptr->len - start_o;
      tmp = ( *close_subchain )( knt_ptr, start_o, end_o );
			TMP_id=KNTLknot_determination ( &tmp, alx_wspace );
			KNTfree_arc(&tmp);
			if ( TMP_id == k_id)
			{
				tmp = ( *close_subchain )( knt_rect, start, end );
				knt_end = KNTpush ( knt_rect, &tmp);
				KNTfree_arc(&tmp);
				// I do not want to pass coordinates 
				free_d2t(knt_end->coord);
				knt_end->coord = NULL;
				KNTarc_SET_value(knt_end, arc_type, ARC_ID_SK, char);
				KNTarc_SET_value(knt_end, Adet_1,knt_rect->Adet_1,int);
				KNTarc_SET_value(knt_end, Adet_2,knt_rect->Adet_2,int);
				strncpy(knt_end->closure,str_cls,strlen(str_cls));
				strncpy(knt_end->simplification,SIMPTOPO,strlen(SIMPTOPO));
				KNTfree_arc ( &arc );
				break;
			}
    }
    // free coordinates in arc and cmp_arc 
    KNTfree_arc ( &arc );
    // reset arc and cmp_arc 
    KNTreset    ( &arc     );
  }

  if(qhull_local_init)
  {
    qhull_terminate();
    qhull_is_init = 0;
  }
  else
  {
    qhull_free_mem();
  }

  free ( arcs_array );

}
*/
/*
 * Auxiliary functions
 */


void buffer_init ( double *** buff,int buffer_dim)
{
  if( *buff == NULL )
  {
    *buff = d2t(buffer_dim,3);
  }
}
void free_buffer ( double *** buff )
{
  if ( *buff != NULL )
  {
    free_d2t ( *buff  );
    *buff = NULL;
  }
}



/*
 * KNTLknot_id
 *
 * given the determinants,
 * assign knot_id according to the table in KNT_defaults.h
 */
int KNTLknot_id ( int dets[2] )
{
  int i;
  int knot_table[KTABLE_LEN] = KTABLE;
  int knot_Adets_table[KTABLE_LEN][2] = KADT_TABLE;

  for ( i = 0 ; i < KTABLE_LEN; i++ )
  {
    if(dets[0] == knot_Adets_table[i][0] && dets[1] == knot_Adets_table[i][1])
    {
      return knot_table[i];
    }
  }
  return K_UNKNWN;
}

int    check_viable_displacement_prd ( double  ** coord, int nsteps, int bead_moved, double *moved_bead_coordinate)
{

  //rispetto alla versione classica, i punti iniziale e finale della catena
  //non sono esclusi e viene utilizzato il modulo (%) di modo che coord[(N+1)%N]=coord[0]

  double pa[3],pb[3],pc[3],p1[3],p2[3],p[3];
  int j, k,ok;

  /* we don't want to consider the possibility to move the first and last points (coinciding) */

  //if ((bead_moved==0) || (bead_moved==(nsteps-1))) return(0);


  /* first triangle */
  for(k=0; k <3; k++){
    pa[k]=coord[(bead_moved-1+nsteps)%nsteps][k];
    pb[k]=coord[bead_moved][k];
    pc[k]=moved_bead_coordinate[k];
  }



  /* set of segments */
  for(j=0; j< (nsteps);  j++)
  {
    p1[0]=coord[j][0];
    p1[1]=coord[j][1];
    p1[2]=coord[j][2];
    p2[0]=coord[(j+1)%nsteps][0];
    p2[1]=coord[(j+1)%nsteps][1];
    p2[2]=coord[(j+1)%nsteps][2];
/*      for(k=0; k <3; k++){
        p1[k]=coord[j][k];
        p2[k]=coord[(j+1)%nsteps][k];
      }*/

      /* exclude from the checks of crossings the bonds involving the nodes "bead-moved-1" and "bead-moved" */
    if (j==((bead_moved-1+nsteps)%nsteps)) continue;
    if (((j+1)%nsteps)==((bead_moved-1+nsteps)%nsteps)) continue;
    if (j==(bead_moved)) continue;
    if (((j+1)%nsteps)==(bead_moved)) continue;

    if(LineFacet(p1,p2,pa,pb,pc,p)==TRUE){ok=0; return(ok);}

    }

  /* second triangle */

  for(k=0; k <3; k++){
    pa[k]=coord[bead_moved][k];
    pb[k]=moved_bead_coordinate[k];
    pc[k]=coord[(bead_moved+1)%nsteps][k];
  }


  /* set of segments */
  for(j=0; j< (nsteps);  j++)
  {
    p1[0]=coord[j][0];
    p1[1]=coord[j][1];
    p1[2]=coord[j][2];
    p2[0]=coord[(j+1)%nsteps][0];
    p2[1]=coord[(j+1)%nsteps][1];
    p2[2]=coord[(j+1)%nsteps][2];
    /*  for(k=0; k <3; k++){
        p1[k]=coord[j][k];
        p2[k]=coord[(j+1)%nsteps][k];
      }
      */

      /* exclude from the checks of crossings the bonds involving the nodes "bead-moved+1" and "bead-moved" */
      if (j==((bead_moved+1)%nsteps)) continue;
      if (((j+1)%nsteps)==(bead_moved+1)) continue;
      if (j==(bead_moved)) continue;
      if (((j+1)%nsteps)==(bead_moved)) continue;

      if(LineFacet(p1,p2,pa,pb,pc,p)==TRUE){ok=0; return(ok);}

    }
  ok=1;
  return(ok);
}

int    check_viable_displacement_sample ( double  ** coord, int * sample, int n_sample, int idx_bead_moved, double *moved_bead_coordinate)
{

  //rispetto alla versione classica, i punti iniziale e finale della catena
  //non sono esclusi e viene utilizzato il modulo (%) di modo che coord[(N+1)%N]=coord[0]

  double pa[3],pb[3],pc[3],p1[3],p2[3],p[3];
  int i,j,j_n, k,ok;
  int bead_moved, p_bead_moved, n_bead_moved;

  p_bead_moved = sample[(idx_bead_moved - 1 + n_sample)%n_sample];
  bead_moved   = sample[idx_bead_moved];
  n_bead_moved = sample[(idx_bead_moved + 1 )%n_sample];

  /* we don't want to consider the possibility to move the first and last points (coinciding) */

  //if ((bead_moved==0) || (bead_moved==(nsteps-1))) return(0);


  /* first triangle */
  for(k=0; k <3; k++){
    pa[k]=coord[p_bead_moved][k];
    pb[k]=coord[bead_moved][k];
    pc[k]=moved_bead_coordinate[k];
  }

  /* set of segments */
  for(i = 0; i< n_sample;  i++)
  {
    j = sample[i];
    j_n = sample[(i+1)%n_sample];
    p1[0]=coord[j][0];
    p1[1]=coord[j][1];
    p1[2]=coord[j][2];
    p2[0]=coord[j_n][0];
    p2[1]=coord[j_n][1];
    p2[2]=coord[j_n][2];
/*      for(k=0; k <3; k++){
        p1[k]=coord[j][k];
        p2[k]=coord[(j+1)%nsteps][k];
      }*/

      /* exclude from the checks of crossings the bonds involving the nodes "bead-moved-1" and "bead-moved" */
    if (i == ((idx_bead_moved-1+n_sample)%n_sample)) continue;
    if (((i+1)%n_sample)==((idx_bead_moved-1+n_sample)%n_sample)) continue;
    if (i == (idx_bead_moved)) continue;
    if ((( i +1)%n_sample)==(idx_bead_moved)) continue;

    if(LineFacet(p1,p2,pa,pb,pc,p)==TRUE){ok=0; return(ok);}

    }

  /* second triangle */

  for(k=0; k <3; k++){
    pa[k]=coord[bead_moved][k];
    pb[k]=moved_bead_coordinate[k];
    pc[k]=coord[n_bead_moved][k];
  }


  /* set of segments */
  for(i=0; i< n_sample;  i++)
  {
    j = sample[i];
    j_n = sample[(i+1)%n_sample];
    p1[0]=coord[j][0];
    p1[1]=coord[j][1];
    p1[2]=coord[j][2];
    p2[0]=coord[j_n][0];
    p2[1]=coord[j_n][1];
    p2[2]=coord[j_n][2];
    /*  for(k=0; k <3; k++){
        p1[k]=coord[j][k];
        p2[k]=coord[(j+1)%nsteps][k];
      }
      */

      /* exclude from the checks of crossings the bonds involving the nodes "bead-moved+1" and "bead-moved" */
      if (i==((idx_bead_moved+1)%n_sample)) continue;
      if (((i+1)%n_sample)==(idx_bead_moved+1)%n_sample) continue;
      if (i==(idx_bead_moved)) continue;
      if (((i+1)%n_sample)==(idx_bead_moved)) continue;

      if(LineFacet(p1,p2,pa,pb,pc,p)==TRUE){ok=0; return(ok);}

    }
  ok=1;
  return(ok);
}
/*
 * KNTLrectify_coord
 *
 * Simplify rings knt_ptr and save the simplified
 * coordinates and relative indexes in a new ring.
 *
 */

KNTarc * KNTLrectify_coord ( KNTarc * knt_ptr, int max_stride )
{
  int       i;
  KNTarc *  knt_rect = NULL;
  double ** dbuffer;
  int     * ibuffer;
  int       len;

  dbuffer  = d2t ( knt_ptr->len, 3 );
  ibuffer  = i1t ( knt_ptr->len    );

  if( knt_ptr->index == NULL )
  {
    for ( i = 0 ; i < knt_ptr->len ; i++ )
    {
      ibuffer[i] = i;
    }
  }
  else
  {
    for ( i = 0 ; i < knt_ptr->len ; i++ )
    {
      ibuffer[i] = knt_ptr->index[i];
    }
  }

  chain_remove_beads_new ( knt_ptr->len, knt_ptr->coord, &len, dbuffer, ibuffer ,max_stride);

  knt_rect = KNTinit_ptr ( );
  KNTadd_coord     ( len, dbuffer, ibuffer, knt_rect );
  KNTarc_SET_value ( knt_rect, start, 0, char );
  KNTarc_SET_value ( knt_rect, end, len, char );
  KNTarc_SET_value ( knt_rect, arc_type, knt_ptr->arc_type, char );
  strncpy ( knt_rect->closure, knt_ptr->closure, strlen(knt_ptr->closure) );
  strncpy ( knt_rect->simplification, SIMPRECT, strlen(SIMPRECT) );

  free_i1t ( ibuffer );
  free_d2t ( dbuffer );

  return knt_rect;
}

KNTarc * KNTLrectify_coord_local ( KNTarc * knt_ptr, int st_p, int end_p, int min_stride, int max_stride )
{
  int       i;
  KNTarc *  knt_rect = NULL;
  double ** dbuffer;
  int     * ibuffer;
  int       len;

  dbuffer  = d2t ( knt_ptr->len, 3 );
  ibuffer  = i1t ( knt_ptr->len    );

  if( knt_ptr->index == NULL )
  {
    for ( i = 0 ; i < knt_ptr->len ; i++ )
    {
      ibuffer[i] = i;
    }
  }
  else
  {
    for ( i = 0 ; i < knt_ptr->len ; i++ )
    {
      ibuffer[i] = knt_ptr->index[i];
    }
  }

  if (max_stride != 0)
  {
    chain_remove_beads_local ( knt_ptr->len, knt_ptr->coord, st_p, end_p, &len, dbuffer, ibuffer ,min_stride,max_stride);
  }
  else
  {
    for(i=0;i<knt_ptr->len;i++)
    {
      dbuffer[i][0]=knt_ptr->coord[i][0];
      dbuffer[i][1]=knt_ptr->coord[i][1];
      dbuffer[i][2]=knt_ptr->coord[i][2];
    }
    len = knt_ptr->len;
  }
  knt_rect = KNTinit_ptr ( );
  KNTadd_coord     ( len, dbuffer, ibuffer, knt_rect );
  KNTarc_SET_value ( knt_rect, start, 0, char );
  KNTarc_SET_value ( knt_rect, end, len, char );
  KNTarc_SET_value ( knt_rect, arc_type, knt_ptr->arc_type, char );
  strncpy ( knt_rect->closure, knt_ptr->closure, strlen(knt_ptr->closure) );
  strncpy ( knt_rect->simplification, SIMPRECT, strlen(SIMPRECT) );

  free_i1t ( ibuffer );
  free_d2t ( dbuffer );

  return knt_rect;
}

/*
 * chain_remove_beads_new
 *
 * Rectify the chain oldcoord[oldlen][3]
 * and store the result in coord[newlen][3].
 * index is an array with the indexes of every bead.
 * if index == NULL the array is generated locally.
 *
 * Using the index array the function is able to
 * smoothly erase beads with incremental stride and
 * to keep track of which are the remaining beads.
 *
 * Rectification procedure: randomly choose a point j.
 * if abs(index[j-1] - index[j+1) < stride) substitute
 * the point j with 0.5*(j-1) + 0.5(j+1). If the substitution
 * does not alter the topology of the whole ring
 * (LineFacet evaluates this condition), accept the substitution
 * and erase point j. Otherwise choose another point.
 * Repeat the procedure len times. set stride = 2*stride
 * and repeat.
 *
 * The procedure continues until no points can be erased at a given
 * stride.
 */


void chain_remove_beads_new ( int oldlen, double ** oldcoord, int *newlen, double ** coord, int * index, int max_stride)
{
  int     i, j, k, iter;
  int     i_start, i_end, lin_dist;
  int     len,templen;
  int     ok, ok1, ok2;
  int     gap, stride;
  int     index_local_init = FALSE;
  int      * tempindex;
  double  ** tempcoord;
  extern long int     * ptr_idum;
#if FOLLOW_BY
  FILE    *fp;
  char    nameout[512];
  int     flwby_stride = 1;
#endif

  /*
   * allocate memory for indexes arrays,
   * if index array is NULL, initialize it.
   */
  len = oldlen;
  tempindex = i1t ( len );

  if( max_stride < 0 || max_stride  > len)
  {
    max_stride = 4*len;
  }
  /*
   * set tempindex to index and stride to the
   * minimum gap in the old chain.
   *
   * In fact I am not setting to the minimum gap,
   * because I can't go around the chain without knowing
   * its original length. That's why the loop stops for i < len - 1
   */
  if ( index != NULL )
  {
    stride = len;
    for( i = 0 ; i < len - 1 ; i++ )
    {
      tempindex [i] = index[i];
      if((gap = abs(index[(i+1)%len]-index[i])) < stride)
      {
        stride = 2*gap;
      }
    }
  }
  else
  {
    index = i1t ( len );
    index_local_init = TRUE;
    stride = 2;
    for ( i = 0 ; i < len ; i++ )
    {
      tempindex[i] = index[i] = i;
    }
  }

  if ( stride < 2 )
  {
    stride = 2;
  }
  /*
   * do the same with the coordinates
   */
  if ( SIMP_BUFFER == NULL )
  {
    tempcoord = d2t ( oldlen, 3 );
  }
  else
  {
    tempcoord = SIMP_BUFFER;
  }
  copy_chain      ( oldcoord, oldlen, coord );
  //len = oldlen;

#if DEBUG == 3
  if(max_stride < oldlen)
    fprintf(stderr, "initial stride %d\n",stride );
#endif
  //stride = 2;
  do
  {
    *newlen = len;
    for( iter = 0 ; iter < *newlen ; iter++ )
    {
      /* pick bead to remove */
      j = (int) floor ( ran2( ptr_idum ) * len );
      i_start = ( j - 1 + len ) % len;
      i_end   = ( j + 1       ) % len;
      copy_chain ( coord, len, tempcoord );
      templen = len;
      //proponi rettificazione
      for( k = 0 ; k < 3 ; k++ )
      {
        tempcoord[j][k] = 0.5 * ( coord[i_start][k] + coord[ i_end ][k]);
      }

      ok = 0;
      lin_dist = ( index[i_end] > index[i_start] ) ? index[i_end] - index[i_start] : index[i_end] - index[i_start] + oldlen;
      //la riga seguente controlla il grado di semplificazione.
      if( abs ( lin_dist ) <= stride)
      {
        ok1 = 1;
#if DEBUG == 3
        if ( max_stride < oldlen )
        fprintf(stderr,"-----------------\nindex j:%10d\nj-1\t%10d\nj+1\t%10d\ndistance%10d\nlen\t%10d\n",
            index[j],index[i_start],index[i_end], abs ( lin_dist ),len);
#endif
      }
      else
      {
        ok1 = 0;
      }
      //valuta tramite LineFacet
      //Se la mossa viene accettata,ricopia la catena temp in quella da semplificare
      //saltando il punto j.La lunghezza della catena va aggiornata di conseguenza.
      if ( ok1 )
        ok2 = check_viable_displacement_prd ( coord, len, j, tempcoord[j] );
      else
        ok2 = 0;
      if ( ok2 )
      {
        templen = 0;
        for( i = 0 ; i < len ; i++ )
        {
          if ( i == j ) continue;
          for( k = 0 ; k < 3 ; k++ )
          {
            tempcoord[templen][k] = coord[i][k];
          }
          tempindex [templen] = index[i];
          templen++;
        }
        ok = 1;
      }

      /* accept */
      if ( ok == 1 )
      {
        copy_chain ( tempcoord, templen, coord );
        len = templen;
        for ( i = 0 ; i < templen ; i++)
        {
          index [i] = tempindex [i];
        }
        if( len < 4 )
        {
          *newlen = len;
          if ( SIMP_BUFFER == NULL )
          {
            free_d2t( tempcoord );
          }
          free_i1t( tempindex );
          if ( index_local_init )
          {
            free_i1t ( index );
          }
          return ;
        }
      }
      /* Reject */
      else
      {
        copy_chain ( coord, len, tempcoord);
        templen = len;
        for ( i = 0 ; i < templen ; i++ )
        {
          tempindex [i] = index[i];
        }
      }

#if FOLLOW_BY
      if ( flwby_stride <= 0 )
        continue;
      if ( ( iter % ( flwby_stride * oldlen )) == 0)
      {
        printf                ( "remove. iteration %5d. length %3d. \n", iter, len);
        printf                ( "Appending configuration to pdbfile \n"           );
        new_dump_coord_to_pdb ( tempcoord, len );
        sprintf               ( nameout, "%s_temp_inflated.xyz", in_file_name     );
        fp      =       fopen ( nameout, "w"   );
        write_configuration   ( fp, coord,  len);
        fclose(fp);
      }
#endif
    }

#if DEBUG == 3
    if ( max_stride < oldlen )
      fprintf(stderr,"used stride = %d -- max_stride = %d\n",stride,max_stride);
#endif
    if( stride * 2 > max_stride && stride < max_stride )
    {
      stride = max_stride;
    }
    else
    {
      stride = stride * 2;
    }
    if(stride > max_stride)
    {
      *newlen = len;
    }
  }while( len != *newlen );
#if DEBUG == 3
  if(max_stride < oldlen)
    fprintf(stderr,"stride = %d\n",stride);

  if(max_stride < oldlen)
  {
    fprintf(stderr,"%d\n",*newlen+1);
    for ( i = 0; i < len ; i++)
      fprintf(stderr,"%4d %9.4lf %9.4lf %9.4lf\n",index[i], coord[i][0], coord[i][1],coord[i][2]);
    fprintf(stderr,"%4d %9.4lf %9.4lf %9.4lf\n",index[0], coord[0][0], coord[0][1],coord[0][2]);
  }
#endif


  if ( SIMP_BUFFER == NULL )
  {
    free_d2t( tempcoord );
  }
  free_i1t( tempindex );
  if ( index_local_init )
  {
    free_i1t ( index );
  }
}
void chain_remove_beads_w_shuffle_nostride ( int oldlen, double ** oldcoord, int *newlen, double ** coord)
{
  int     i, j, k;
  int     temp, range;
  int     i_start, i_remove, i_end;
  int     len;
  int      * tempindex;
  int       * index;
  double   moved_bead[3];
  extern long int     * ptr_idum;

  /*
   * allocate memory for indexes arrays,
   * if index array is NULL, initialize it.
   */
  tempindex = i1t ( oldlen );
  index     = i1t ( oldlen );
  *newlen = len = oldlen;

  /*
   * set tempindex to index and stride to the
   * minimum gap in the old chain.
   *
   * In fact I am not setting to the minimum gap,
   * because I can't go around the chain without knowing
   * its original length. That's why the loop stops for i < len - 1
   */
  for ( i =0 ; i < oldlen ;i++ )
  {
    tempindex[i] = i;
    index[i]  = i;
  }

  /*
   * do the same with the coordinates
   */
  do
  {
    *newlen = len;
    if ( len < 5 ) { break; } //no need to further simplify the chain..
    //FY SHUFFLE of index till len
    for ( j = len-1 ; j > 0 ; j--)
    {
      //from the last idx to the next-to-first
      //ie for indices 0..N-1 we go from N-1 to 1
      range = j+1;
      //cerr << "Range is:\t" << range << endl;
      k = (int) floor ( ran2( ptr_idum ) * range );
      temp = tempindex[k];
      tempindex[k] = tempindex[j];
      tempindex[j] = temp;
    }
    i = 0;
    do
    {
      /*fprintf(stderr,"%d\n",oldlen);
      for ( k = 0; k < len; k++ )
      {
        fprintf(stderr,"%lf %lf %lf\n",oldcoord[index[k]][0],oldcoord[index[k]][1],oldcoord[index[k]][2]);
      }
      for ( k = len; k < oldlen; k++ )
      {
        fprintf(stderr,"%lf %lf %lf\n",oldcoord[0][0],oldcoord[0][1],oldcoord[0][2]);
      }*/
      //Now I can walk through the shuffled array.
      i_start = ( tempindex[i]-1+len )%len;
      i_end   = ( tempindex[i]+1 )%len;
      i_start = index[i_start];
      i_end   = index[i_end];
      i_remove = tempindex[i];
      for( k = 0 ; k < 3 ; k++ )
      {
        moved_bead[k] = 0.5 * ( oldcoord[i_start][k] + oldcoord[ i_end ][k]);
      }
      if ( check_viable_displacement_sample ( oldcoord, index, len, i_remove, moved_bead))
      {
        //update reference vector tempindex
        for ( j = 0; j < len ; j++ )
        {
          if ( tempindex[j] > i_remove )
          {
            tempindex[j]--;
          }
        }
        //erase i_remove from index
        for ( j = i_remove + 1; j < len ; j++)
        {
          index[j-1] = index[j];
        }
        //move last tempindex to current position, to analyze at next step.
        tempindex[i] = tempindex[ len - 1 ];
        //reduce dimension of the sample to be analyzed (i.e. erasing moved_bead from the sample!)
        len--;
      }
      else
      {
        i++;
      }

    }while ( i != len && len>=5);
    //now increase the stride
  }while ( len != *newlen);
  //copy the coordinates!
  for ( i = 0 ; i < len ; i++ )
  {
    j = index[i];
    coord[i][0] = oldcoord[j][0];
    coord[i][1] = oldcoord[j][1];
    coord[i][2] = oldcoord[j][2];
  }
  free_i1t( tempindex );
  free_i1t( index );
}
/*
void chain_remove_beads_w_shuffle ( int oldlen, double ** oldcoord, int *newlen, double ** coord,  int max_stride)
{
  int     i, j, k;
  int     temp, range;
  int     i_start, i_remove, i_end, lin_dist;
  int     len;
  int      stride;
  int      * tempindex;
  int       * index;
  double   moved_bead[3];
  extern long int     * ptr_idum;
#if FOLLOW_BY
  FILE    *fp;
  char    nameout[512];
  int     flwby_stride = 1;
#endif

  //
  // allocate memory for indexes arrays,
  // if index array is NULL, initialize it.
  //
  tempindex = i1t ( oldlen );
  index     = i1t ( oldlen );
  *newlen = len = oldlen;

  if( max_stride < 0 || max_stride  > oldlen)
  {
    max_stride = 4*oldlen;
  }
  //
  // set tempindex to index and stride to the
  // minimum gap in the old chain.
  //
  // In fact I am not setting to the minimum gap,
  // because I can't go around the chain without knowing
  // its original length. That's why the loop stops for i < len - 1
  //
  for ( i =0 ; i < oldlen ;i++ )
  {
    tempindex[i] = i;
    index[i]  = i;
  }

  stride = 2;
  //
  // do the same with the coordinates
  //
  do
  {
    *newlen = len;
    if ( len < 5 ) { break; } //no need to further simplify the chain..
    //FY SHUFFLE of index till len
    for ( j = len-1 ; j > 0 ; j--)
    {
      //from the last idx to the next-to-first
      //ie for indices 0..N-1 we go from N-1 to 1
      range = j+1;
      //cerr << "Range is:\t" << range << endl;
      k = (int) floor ( ran2( ptr_idum ) * range );
      temp = tempindex[k];
      tempindex[k] = tempindex[j];
      tempindex[j] = temp;
    }
    i = 0;
    do
    {
      //fprintf(stderr,"%d\n",oldlen);
      //for ( k = 0; k < len; k++ )
      //{
      //  fprintf(stderr,"%lf %lf %lf\n",oldcoord[index[k]][0],oldcoord[index[k]][1],oldcoord[index[k]][2]);
      //}
      //for ( k = len; k < oldlen; k++ )
      //{
      //  fprintf(stderr,"%lf %lf %lf\n",oldcoord[0][0],oldcoord[0][1],oldcoord[0][2]);
      //}
      //Now I can walk through the shuffled array.
      i_start = ( tempindex[i]-1+len )%len;
      i_end   = ( tempindex[i]+1 )%len;
      lin_dist = ( index[i_end] > index[i_start] ) ? index[i_end] - index[i_start] : index[i_end] - index[i_start] + oldlen;
      if ( abs(lin_dist) > stride )
      {
        i++;
        continue;
      }
      else
      {
        i_start = index[i_start];
        i_end   = index[i_end];
        i_remove = tempindex[i];
        for( k = 0 ; k < 3 ; k++ )
        {
          moved_bead[k] = 0.5 * ( oldcoord[i_start][k] + oldcoord[ i_end ][k]);
        }
        if ( check_viable_displacement_sample ( oldcoord, index, len, i_remove, moved_bead))
        {
          //update reference vector tempindex
          for ( j = 0; j < len ; j++ )
          {
            if ( tempindex[j] > i_remove )
            {
              tempindex[j]--;
            }
          }
          //erase i_remove from index
          for ( j = i_remove + 1; j < len ; j++)
          {
            index[j-1] = index[j];
          }
          //move last tempindex to current position, to analyze at next step.
          tempindex[i] = tempindex[ len - 1 ];
          //reduce dimension of the sample to be analyzed (i.e. erasing moved_bead from the sample!)
          len--;
        }
        else
        {
          i++;
        }
      }
    }while ( i != len );
    //now increase the stride
    if( stride * 2 > max_stride && stride < max_stride )
    {
      stride = max_stride;
    }
    else
    {
      stride = stride * 2;
    }
  }while ( len != *newlen);
  //copy the coordinates!
  for ( i = 0 ; i < len ; i++ )
  {
    j = index[i];
    coord[i][0] = oldcoord[j][0];
    coord[i][1] = oldcoord[j][1];
    coord[i][2] = oldcoord[j][2];
  }
  free_i1t( tempindex );
  free_i1t( index );
}
*/

int chain_remove_beads_local ( int oldlen, double ** oldcoord,int start,int end, int *newlen, double ** coord, int * index,int min_stride, int max_stride)
{
  int     i, j, k, iter;
  int     len,templen;
  int     lin_dist;
  int     i_start,i_end;
  int     seg_len;
  int     ok, ok1;
  int     gap, stride;
  int     index_local_init = FALSE;
  int      * tempindex;
  double  ** tempcoord;
  extern long int     * ptr_idum;
#if FOLLOW_BY
  FILE    *fp;
  char    nameout[512];
  int     flwby_stride = 1;
#endif

  /*
   * alloc memory for indexes arrays,
   * if index array is NULL, initialize it.
   */
  len = oldlen;
  tempindex = i1t ( len );
  seg_len = start > end ?  len - start + end : end - start;
  if( max_stride < 0 || max_stride  > seg_len)
  {
    max_stride = 4*seg_len;
  }

  /*
   * set tempindex to index and stride to the
   * minimum gap in the old chain.
   *
   * In fact I am not setting to the minimum gap,
   * because I can't go around the chain without knowing
   * its original length. That's why the loop stops for i < len - 1
   */
  if ( index != NULL )
  {
    stride = len;
    for( i = 0 ; i < len - 1 ; i++ )
    {
      tempindex [i] = index[i];
      if((gap = abs(index[(i+1)%len]-index[i])) < stride)
      {
        stride = 2*gap;
      }
    }
    tempindex [len-1] = index[len-1]; //QUESTO MANCAVA!
  }
  else
  {
    index = i1t ( len );
    index_local_init = TRUE;
    stride = 2;
    for ( i = 0 ; i < len ; i++ )
    {
      tempindex[i] = index[i] = i;
    }
  }

  if ( stride < 2 )
  {
    stride = 2;
  }

  if ( min_stride != DONT_CARE)
  {
    stride = min_stride;
  }

  /*
   * do the same with the coordinates
   */
  tempcoord = d2t ( oldlen, 3 );
  copy_chain      ( oldcoord, oldlen, coord );
  //len = oldlen;

#if DEBUG == 3
  fprintf(stderr, "initial stride %d\n",stride );
#endif
  //stride = 2;
  do
  {
    *newlen=len;
    for( iter = 0 ; iter < *newlen ; iter++ )
    {
      /* pick bead to remove */
      j = ((int) floor ( ran2( ptr_idum ) * seg_len ) + start) % len;
      i_start = ( j - 1 + len ) % len;
      i_end   = ( j + 1       ) % len;
      copy_chain ( coord, len, tempcoord );
      templen = len;
      //proponi rettificazione
      for( k = 0 ; k < 3 ; k++ )
      {
        tempcoord[j][k] = 0.5 * ( coord[( j - 1 + len ) % len][k] + coord[ ( j + 1 ) % len][k]);
      }

      ok = 0;
      lin_dist = ( index[i_end] > index[i_start] ) ? index[i_end] - index[i_start] : index[i_end] - index[i_start] + oldlen;
      //la riga seguente controlla il grado di semplificazione.
      if( abs ( lin_dist ) <= stride)
        ok1 = 1;
      else
        ok1 = 0;
      //valuta tramite LineFacet
      //Se la mossa viene accettata,ricopia la catena temp in quella da semplificare
      //saltando il punto j.La lunghezza della catena va aggiornata di conseguenza.
      if ( ok1 && (check_viable_displacement_prd ( coord, len, j, tempcoord[j] ) == 1 ) && j != start && index[j] != end)
      {
        templen = 0;
        for( i = 0 ; i < len ; i++ )
        {
          if ( i == j ) continue;
          for( k = 0 ; k < 3 ; k++ )
          {
            tempcoord[templen][k] = coord[i][k];
          }
          tempindex [templen] = index[i];
          templen++;
        }
        ok = 1;
        seg_len--;
        if ( j < start )
          start = ( start - 1 + len )%len;
      }

      /* accept */
      if ( ok == 1 )
      {
        copy_chain ( tempcoord, templen, coord );
        len = templen;
        for ( i = 0 ; i < templen ; i++)
        {
          index [i] = tempindex [i];
        }
        if( len < 4 )
        {
          *newlen = len;
          free_d2t( tempcoord );
          free_i1t( tempindex );
          if ( index_local_init )
          {
            free_i1t ( index );
          }
          return seg_len;
        }
      }
      /* Reject */
      else
      {
        copy_chain ( coord, len, tempcoord);
        templen = len;
        for ( i = 0 ; i < templen ; i++ )
        {
          tempindex [i] = index[i];
        }
      }

#if FOLLOW_BY
      if ( flwby_stride <= 0 )
        continue;
      if ( ( iter % ( flwby_stride * oldlen )) == 0)
      {
        printf                ( "remove. iteration %5d. length %3d. \n", iter, len);
        printf                ( "Appending configuration to pdbfile \n"           );
        new_dump_coord_to_pdb ( tempcoord, len );
        sprintf               ( nameout, "%s_temp_inflated.xyz", in_file_name     );
        fp      =       fopen ( nameout, "w"   );
        write_configuration   ( fp, coord,  len);
        fclose(fp);
      }
#endif
    }
    //fprintf(stderr, "\n"); //RAN2 DEBUG
#if DEBUG == 3
    if ( max_stride < oldlen )
      fprintf(stderr,"used stride = %d -- max_stride = %d\n",stride,max_stride);
#endif
    if( stride * 2 > max_stride && stride < max_stride )
    {
      stride = max_stride;
    }
    else
    {
      stride = stride * 2;
    }
    if(stride > max_stride)
    {
      *newlen = len;
    }

  }while( len != *newlen );

  free_d2t( tempcoord );
  free_i1t( tempindex );
  if ( index_local_init )
  {
    free_i1t ( index );
  }
  return seg_len;
}



void copy_chain(double ** coord,int len, double ** coordnew)
{
  int i;
  for ( i = 0 ; i < len ; i++ )
  {
    coordnew[i][0] = coord[i][0];
    coordnew[i][1] = coord[i][1];
    coordnew[i][2] = coord[i][2];
  }
}


int compare_len( const void *a , const void *b )
{
  const KNTarc * ia = (const KNTarc * ) a;
  const KNTarc * ib = (const KNTarc * ) b;
  return ia->len - ib->len;
}

int compare_adets( const void *a , const void *b )
{
  const KNTadets * ia = (const KNTadets * ) a;
  const KNTadets * ib = (const KNTadets * ) b;
  if ( ia->Adets[0] == ib->Adets[0] && ia->Adets[1] == ib->Adets[1] )
    return 0;
  else
    return -1;
}

int compare_kid ( const void *a, const void *b )
{
  const KNTadets * ia = (const KNTadets * ) a;
  const KNTadets * ib = (const KNTadets * ) b;
  if ( ia->k_id == ib->k_id )
    return 0;
  else
    return -1;
}
/*
 * push a new element on top of stack
 */
void push(node **head, KNTadets knot)
{
    node * n = ( node * ) malloc(sizeof(node));
    if ( n == NULL )
    {
      failed("failed to allocate memory in push.ABORT");
    }
    n->next = *head;
    n->knot.Adets[0]=knot.Adets[0];
    n->knot.Adets[1]=knot.Adets[1];
    n->knot.k_id=knot.k_id;
    *head = n;
}

/*
 * recover top element from top of stack
 */
KNTadets pop ( node ** head )
{
  KNTadets data;
  node * n;
  if ( *head == NULL )
  {
    fprintf(stderr,"trying to pop an empty stack.\nABORT\n");
    exit(1);
  }
  n = *head;
  data.Adets[0] = n->knot.Adets[0];
  data.Adets[1] = n->knot.Adets[1];
  data.k_id = n->knot.k_id;
  *head = n->next;
  free(n);
  return data;
}


/*
 * Check if the arc give by start,end is included in the
 * arc given by st_p, end_p.
 */
int check_inclusion ( int start, int end, int st_p, int end_p )
{
  if (st_p < end_p )
  {
    if ( start <= end && start >=st_p && end <=end_p) {return TRUE;}
    else { return FALSE;}
  }
  else if (st_p > end_p )
  {
    if ( start<=end && end<=end_p) { return TRUE;}
    else if ( start > end && start>=st_p && end<=end_p) {return TRUE;}
    else {return FALSE;}
  }
  else
  {
    return FALSE;
  }
  failed("unrecognized condition in check_inclusione..\n");
}

/*
 * KNTLreset_random_closure
 *
 * allow the user to reset the default values used in random closures.
 * If negative values are given in input keeps the corresponding
 * default values.
 *
 * Check the beginning of this file for default values.
 */
void KNTLreset_random_closure ( int N_cls, double R_fact, double tresh )
{
  extern int    stch_n_closures ;
  extern double stch_close_fact;
  extern double stch_treshold;

  stch_n_closures = ( N_cls  > 0 ) ? N_cls  : stch_n_closures;
  stch_close_fact = ( R_fact > 0 ) ? R_fact : stch_close_fact;
  stch_treshold   = ( tresh  > 0 ) ? tresh  : stch_treshold ;
}


void KNTLrotate ( KNTarc * knt_ptr, double axis[3], double theta)
{
  int i;
  double CsTh, SnTh,OnemCsTh;
  double nx,ny,nz;
  gsl_vector *v = gsl_vector_alloc (3);
  gsl_vector *w = gsl_vector_alloc (3);
  gsl_matrix *R = gsl_matrix_alloc(3,3);

  if ( knt_ptr == NULL )
  {
    failed("tryng to rotate a NULL knt_struct!\n");
  }
  if ( knt_ptr->is_init == FALSE )
  {
    failed("tryng to rotate a non initialized knt_struct!\n");
  }
  if ( knt_ptr->coord == NULL )
  {
    failed("tryng to rotate an empty knt_struct!\n");
  }

  normalize_d(axis,3);
  nx = axis[0]; ny = axis[1]; nz = axis[2]; // I know it is dumb, just reusing code..
  CsTh = cos(theta);
  SnTh = sin(theta);
  OnemCsTh = 1 - CsTh;
  //random versor for PIVOT rotation |n| = 1
  //rotation matrix, from quaternions multiplication
  //first row
  gsl_matrix_set ( R,0,0,CsTh + nx*nx* OnemCsTh);
  gsl_matrix_set ( R,0,1,nx*ny*OnemCsTh - nz*SnTh);
  gsl_matrix_set ( R,0,2,nx*nz*OnemCsTh + ny*SnTh);
  //second row
  gsl_matrix_set ( R,1,0,nx*ny*OnemCsTh +nz*SnTh);
  gsl_matrix_set ( R,1,1,CsTh + ny*ny*OnemCsTh);
  gsl_matrix_set ( R,1,2,ny*nz*OnemCsTh - nx*SnTh);
  //third row
  gsl_matrix_set ( R,2,0,nx*nz*OnemCsTh - ny*SnTh);
  gsl_matrix_set ( R,2,1,ny*nz*OnemCsTh + nx*SnTh);
  gsl_matrix_set ( R,2,2,CsTh + nz*nz*OnemCsTh);
  for ( i = knt_ptr->len-1; i >=0 ; i-- )
  {
    knt_ptr->coord[i][0] -=knt_ptr->coord[0][0];
    knt_ptr->coord[i][1] -=knt_ptr->coord[0][1];
    knt_ptr->coord[i][2] -=knt_ptr->coord[0][2];
  }
  for ( i = knt_ptr->len-1; i >0 ; i-- )
  {
    gsl_vector_set ( v, 0, knt_ptr->coord[i][0] - knt_ptr->coord[0][0]);
    gsl_vector_set ( v, 1, knt_ptr->coord[i][1] - knt_ptr->coord[0][1]);
    gsl_vector_set ( v, 2, knt_ptr->coord[i][2] - knt_ptr->coord[0][2]);
    gsl_blas_dgemv ( CblasNoTrans, 1, R, v, 0,w);

    knt_ptr->coord[i][0] = gsl_vector_get(w,0);
    knt_ptr->coord[i][1] = gsl_vector_get(w,1);
    knt_ptr->coord[i][2] = gsl_vector_get(w,2);
  }
	gsl_matrix_free(R);
	gsl_vector_free(v);
	gsl_vector_free(w);
}

void KNTLrotate_random ( KNTarc * knt_ptr )
{
	double s;
	double V1;
	double V2;
	double dR;
	double axis[3];
	double theta;
  extern long int     * ptr_idum;
  if ( knt_ptr == NULL )
  {
    failed("tryng to rotate a NULL knt_struct!\n");
  }
  if ( knt_ptr->is_init == FALSE )
  {
    failed("tryng to rotate a non initialized knt_struct!\n");
  }
  if ( knt_ptr->coord == NULL )
  {
    failed("tryng to rotate an empty knt_struct!\n");
  }
	//V1,V2 uniformly distributed between (-1,1), so that S=V1^2+V2^2<1
	do{
		V1=2*ran2(ptr_idum)-1;
		V2=2*ran2(ptr_idum)-1;
		s=V1*V1+V2*V2;
	}while(s>=1);
	dR=ran2(ptr_idum)*2.0;
	axis[0]=dR* 2*V1*sqrt(1-s);
	axis[1]=dR* 2*V2*sqrt(1-s);
	axis[2]=dR* (1-2*s);
	theta=ran2(ptr_idum)*PI;
	KNTLrotate(knt_ptr,axis,theta);
}
