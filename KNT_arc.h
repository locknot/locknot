/*************************************************/
/*14-12-2010 Luca Tubiana                        */
/*This header provides definitions and prototypes*/
/*to implement the struct knot_arc and use it    */
/*in a program with hdf5 file i/o                */
/*************************************************/

#ifndef HDR_KNTARC
#define HDR_KNTARC

//#include <hdf5.h>
//#include <hdf5_hl.h>
#include <stdlib.h>
#include <string.h>
#include "my_memory.h"
//#include "my_geom.h"
#include "KNT_defaults.h"
#include "messages.h"

/***MACROS********************************************/
#define KNTarc_SET_value(ptr,name, val, type) \
  (ptr)->name = ( type ) ( val ); \
  (ptr)->flag_##name = TRUE;

#define KNTarc_isSET(ptr,name) \
  (ptr)->flag_##name

#define KNTarc_SETif(ptr, name, val, type) \
  if( (int) val != (int) DONT_CARE ) \
  { \
    (ptr)->name = ( type ) ( val ); \
    (ptr)->flag_##name = TRUE; \
  }


#define KNTarc_GETif(ptr,name, out, type) \
  if( (ptr)->flag_##name ) \
  {                    \
    out = ( type ) (ptr)->name; \
  } \
  else \
  { \
    out = DONT_CARE; \
  }


#define KNTarc_isALLOC(ptr) \
  (ptr)->is_alloc

/*
 * The macro KNTarc_SET_value will not work if an argument like (double*) is passed.
 *Therefore one needs to typedef all pointers.
 */
typedef  double * double_ptr;
typedef  float  * float_ptr;
typedef  int    * int_ptr;
typedef  long   * long_ptr;
typedef  short  * short_ptr;

/**************************************************************/
/*The following structure contains the main                   */
/*variables used to define the geometric and                  */
/*topological properties of a knotted arc or                  */
/*ring. It is structured as a node of a linked                */
/*list in order to allow storage of the whole                 */
/*ring and any subarc of interest.                            */
/*This scheme will probably turn out to be                    */
/*useful for other purposes in the future                     */
/**************************************************************/

struct knot_arc {
  char        arc_type;        /*identifier for arc_type. RING, shortest_knotted, shortest_c_knotted, any arc*/
  int         start, end;      /*start and end of arc (0, len) if ring or linear                             */
  int         len;             /*arc's length. coord[len][3];                                                */
  double   ** coord;           /*arc's coordinates                                                           */
  int       * index;           /*arc  bead's indexes. Keep trace of the effects of a simplification.         */
  short    ** kntmatrix;       /*knot matrix. Use carefully...                                               */
  char        closure[STR_OUT_MAXLEN];         /*a string containing an explanation of the closure."ring" if it is a ring.   */
  char        type[STR_OUT_MAXLEN];            /*knot type (3_1, 4_1, 5_1, 3_1#3_1..etc)                                     */
  char        simplification[STR_OUT_MAXLEN];  /*simplification scheme adopted                                               */
  int         Adet_1, Adet_2;  /*Alexander determinants in -1 and -2                                         */
  double      cross, scross;   /*average crossing number and average signed crossing number    (Unused)      */
  double      writhe;          /*writhe of the ring                       (Unused)                           */

  double      ch_area, ch_vol; /* Convex hull area and volume (Unused)                                       */
  double      A,P,Rg;          /*Asphericity, Prolateness, Gyration radius (Unused)                          */
  double      Rc;              /*Radius of confining sphere.       (Unused)                                  */
  double      cm[3];           /*center of mass.                                                             */
  /* flags. 0: value unassigned. >0 value set. use to save data.      */
  short       is_alloc,is_init;

  short       flag_arc_type;
  short       flag_len,flag_start, flag_end;
  short       flag_Adet_1, flag_Adet_2;
  short       flag_cross, flag_scross, flag_writhe;
  short       flag_ch_area, flag_ch_vol;
  short       flag_A, flag_P, flag_Rg, flag_Rc, flag_cm;

  struct knot_arc * next;   /*pointer to another arc*/
};

/*-----------------------------------------------------------------------------------------------------------
 * struct knot_arc_props.
 *
 * Defined to print and read ring properties from hdf5 files.
 * Properties are stored in table format, the present class is used simply as a buffer for i/o operations
 *
 */
struct knot_arc_props {
  char    arc_type;                        /*identifier for arc_type. RING, shortest_knotted, shortest_c_knotted, any arc*/
  char    simplification[STR_OUT_MAXLEN];  /*simplification scheme adopted                                               */
  int     start, end;                      /*start and end of arc (0, len) if ring                                       */
  double  ch_area, ch_vol;                 /* Convex hull area and volume                                                */
  double  A,P,Rg;                          /*Asphericity, Prolateness, Gyration radius                                   */
  double  Rc;                              /*Radius of confining sphere.                                                 */
  double  cm[3];                           /*center of mass.                                                             */

  short   is_init;

  char    closure[STR_OUT_MAXLEN];         /*a string containing an explanation of the closure."ring" if it is a ring.   */
  int     Adet_1, Adet_2;                  /*Alexander determinants in -1 and -2                                         */
  char    type[STR_OUT_MAXLEN];            /*knot type (3_1, 4_1, 5_1, 3_1#3_1..etc)                                     */
  double  cross, scross;                   /*average crossing number and average signed crossing number                  */
  double  writhe;                          /*writhe of the ring                                                          */
};



typedef struct knot_arc       KNTarc;
typedef struct knot_arc_props KNTarc_props;


/* PROTOTYPES-----------------------------*/
/*--initialization and memory allocation--*/
void        KNTreset           ( KNTarc * );
void        KNTremove          ( KNTarc * );
KNTarc    * KNTpush            ( KNTarc *, KNTarc * );
KNTarc    * KNTlast            ( KNTarc * );
KNTarc      KNTinit            (          );
KNTarc    * KNTinit_ptr        (          );
KNTarc      KNTinit_matrix     ( int      );
KNTarc    * KNTinit_matrix_ptr ( int      );
void        KNTadd_matrix      ( KNTarc * );
void        KNTadd_coord       ( int, double **, int *, KNTarc * );
void KNTprops_reset(KNTarc_props * knot_props);
KNTarc_props KNTprops_init     (          );
KNTarc_props KNTprops_init_ptr (          );
/*--copy------------------------------------*/
KNTarc    * KNTcopy            ( KNTarc * );
KNTarc    * KNTcopy_el            ( KNTarc * );
void        KNT_props_ccopy    (KNTarc * , KNTarc_props * );
void        KNT_props_cset     (KNTarc_props * , KNTarc * );
/*--free memory-----------------------------*/
void        KNTfree_arc        ( KNTarc * );




#endif /*HDR_KNTARC */
